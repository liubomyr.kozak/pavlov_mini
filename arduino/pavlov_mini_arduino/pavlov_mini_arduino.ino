#include <ros.h>
#include <Wire.h>

#include <sensor_msgs/JointState.h>
#include <pavlov_mini_msgs/set_motor_calibration.h>
#include <pavlov_mini_msgs/connect_servos.h>

#include <std_msgs/Float64.h>
#include <sensor_msgs/Imu.h>

#include "SparkFun_BNO080_Arduino_Library.h"



#include "configuration.h"
#include "actuator.h"
#include <Servo.h>








/*********************************************
************* PINS CONFIGURATION *************
**********************************************/


char *servoNames[] = {"hip1_fl", "hip2_fl", "knee_fl",
                      "hip1_fr", "hip2_fr", "knee_fr",
                      "hip1_bl", "hip2_bl", "knee_bl",
                      "hip1_br", "hip2_br", "knee_br"};




int PIN_SERVOS[NUM_MOTORS]  = {2,  1,  0,
                               5,  4,  3,
                               8,  7,  6,
                               11,  10, 9};



int PIN_SERVO_SWITCH = 14;

/***************** MAIN VARIABLES *****************/
Actuator* servomotors[NUM_MOTORS];
ros::NodeHandle  nh;


                                  
BNO080 imuSensor;  // Check I2C device address and correct line below (by default address is 0x29 or 0x28)id, address






/***************** LOGS *****************/
void logInfo(String msg){
    nh.loginfo(msg.c_str());
}

void logError(String msg){
    nh.logerror(msg.c_str());
}



/*********************************************
************* CALLBACKS FUNCTIONS ************
**********************************************/


void setMotorCalCB(const pavlov_mini_msgs::set_motor_calibration::Request & req, pavlov_mini_msgs::set_motor_calibration::Response & res){
  int i = req.servo_index;

  if (!req.setCalibration_servo){
    servomotors[i]->setServoCalibration(false);
  }
  
  else{
    servomotors[i]->setServoCalibration(req.min_calAngle,  req.max_calAngle,
                                        req.min_posServo,  req.max_posServo,
                                        req.min_posAngle,  req.max_posAngle);
                                        
    servomotors[i]->setLimPositions(req.min_lim_angle, req.max_lim_angle);

  }

                                 
  res.result = true;
  logInfo( "Calibration for motor " + servomotors[i]->getName() + " (" + String(i) + "),  Servo cal: " + String( servomotors[i]->isServoCalibrated()));

}



void connectServosCB(const pavlov_mini_msgs::connect_servos::Request & req, pavlov_mini_msgs::connect_servos::Response & res){
  bool connect = req.connect;
  if (connect){
      for(short i=0; i < NUM_MOTORS; i++){
        servomotors[i]->connect();
      }
      res.result = true;
      logInfo("Servomotors attached");
      digitalWrite(PIN_SERVO_SWITCH, HIGH);
      
  }
  else{
      for(short i=0; i < NUM_MOTORS; i++){
          servomotors[i]->disconnect();
      }
      res.result = true;
      logInfo("Servomotors disattached");
      digitalWrite(PIN_SERVO_SWITCH, LOW);


  }


  
}



void jointControlCB(const sensor_msgs::JointState& msg){


  if(msg.position_length != NUM_MOTORS){
    logError("Invalid joint control msg. Either position array or effort array is empty!.");
    return;
  }

  for(short i=0; i < NUM_MOTORS; i++){
      servomotors[i]->setPosition(msg.position[i]);
  }
}




/************************ ROS SERVICES ************************/

ros::ServiceServer<pavlov_mini_msgs::set_motor_calibration::Request, pavlov_mini_msgs::set_motor_calibration::Response> setMotorCalServer("pavlov_mini/set_motor_calibration",&setMotorCalCB);
ros::ServiceServer<pavlov_mini_msgs::connect_servos::Request, pavlov_mini_msgs::connect_servos::Response> connectServosServer("pavlov_mini/connect_servos",&connectServosCB);


/************************ ROS SUBSCRIBERS ************************/

ros::Subscriber<sensor_msgs::JointState> jointControlSub("pavlov_mini/joints_control", &jointControlCB );


/************************ ROS PUBLISHERS ************************/
sensor_msgs::JointState joint_state;
sensor_msgs::Imu imu_state;
std_msgs::Float64 loop_freq;

ros::Publisher jointStatePub("pavlov_mini/joints_state", &joint_state);
ros::Publisher IMUStatePub("pavlov_mini/imu_data",       &imu_state);
ros::Publisher freqInfoPub("pavlov_mini/driver_freq",    &loop_freq);



/************************ TIMERS ************************/
unsigned long Timer;
unsigned long TimerPublisher;
unsigned long TimerFreqPubliser;

unsigned int counts_loop;





/***************************************************************
 * ********************** SETUP FUNCTION ***********************
 ***************************************************************/


void setup() {


  nh.initNode();
  nh.advertise(jointStatePub);
  nh.advertise(IMUStatePub);
  nh.advertise(freqInfoPub);

  nh.advertiseService(setMotorCalServer);
  nh.advertiseService(connectServosServer);

  nh.subscribe(jointControlSub);



  Wire.begin();
  imuSensor.begin();
  Wire.setClock(400000); //Increase I2C data rate to 400kHz
  
  imuSensor.enableGyroIntegratedRotationVector(BNO080_SAMPLERATE_DELAY_MS);
  imuSensor.enableLinearAccelerometer(BNO080_SAMPLERATE_DELAY_MS);  


  
  
  Timer             = micros();
  TimerPublisher    = micros();
  TimerFreqPubliser = micros();

  joint_state.name_length     = NUM_MOTORS;
  joint_state.name            = servoNames;
  joint_state.position_length = NUM_MOTORS;
  joint_state.velocity_length = NUM_MOTORS;
  joint_state.effort_length   = NUM_MOTORS;

  for(short i=0; i < NUM_MOTORS; i++){
      servomotors[i] = new Actuator(&nh, i, servoNames[i], PIN_SERVOS[i]);
      //servomotors[i]->connect();
      //servomotors[i]->disconnect();
  }

  pinMode(PIN_SERVO_SWITCH, OUTPUT);

  
}








void getImuData(){
  if (imuSensor.dataAvailable() == false) {
      //logError("Teensy: Cannot connect to imu sensor");
        return; 
     }
    
    float q[4];
    q[0] =   imuSensor.getQuatJ();
    q[1] = - imuSensor.getQuatReal();
    q[2] = - imuSensor.getQuatK();
    q[3] = - imuSensor.getQuatI();

    //BLA::Matrix<3,3> rotMatrix = getRotationMatrixFromQuaternion(q);

    imu_state.orientation.w = -q[0];
    imu_state.orientation.x =  q[1];
    imu_state.orientation.y =  q[2];
    imu_state.orientation.z = -q[3];

    imu_state.angular_velocity.x =  -imuSensor.getFastGyroY();
    imu_state.angular_velocity.y =  -imuSensor.getFastGyroX();
    imu_state.angular_velocity.z =   imuSensor.getFastGyroZ();


    imu_state.linear_acceleration.x =  -imuSensor.getLinAccelY();
    imu_state.linear_acceleration.y =  -imuSensor.getLinAccelX();
    imu_state.linear_acceleration.z =   imuSensor.getLinAccelZ();

    


    /*
    ang_vel_x[COV_MATRIX_SAMPLE - 1] = gyro(0,0); //gyro_rotated[0];
    ang_vel_y[COV_MATRIX_SAMPLE - 1] = gyro(1,0); //gyro_rotated[1];
    ang_vel_z[COV_MATRIX_SAMPLE - 1] = gyro(2,0); //gyro_rotated[2];


    imu_state.angular_velocity.x = ang_vel_x[COV_MATRIX_SAMPLE - 1];
    imu_state.angular_velocity.y = ang_vel_y[COV_MATRIX_SAMPLE - 1];
    imu_state.angular_velocity.z = ang_vel_z[COV_MATRIX_SAMPLE - 1];

    imu_state.angular_velocity_covariance[0] = covArrays(ang_vel_x, ang_vel_x);
    imu_state.angular_velocity_covariance[1] = covArrays(ang_vel_x, ang_vel_y);
    imu_state.angular_velocity_covariance[2] = covArrays(ang_vel_x, ang_vel_z);
    imu_state.angular_velocity_covariance[3] = imu_state.angular_velocity_covariance[1];
    imu_state.angular_velocity_covariance[4] = covArrays(ang_vel_y, ang_vel_y);
    imu_state.angular_velocity_covariance[5] = covArrays(ang_vel_y, ang_vel_z);
    imu_state.angular_velocity_covariance[6] = imu_state.angular_velocity_covariance[2];
    imu_state.angular_velocity_covariance[7] = imu_state.angular_velocity_covariance[5];
    imu_state.angular_velocity_covariance[8] = covArrays(ang_vel_z, ang_vel_z);




    lin_acc_x[COV_MATRIX_SAMPLE - 1] =  acc(0,0); //acc_rotated[0];
    lin_acc_y[COV_MATRIX_SAMPLE - 1] =  acc(1,0); //acc_rotated[1];
    lin_acc_z[COV_MATRIX_SAMPLE - 1] = -acc(2,0); //acc_rotated[2];



    imu_state.linear_acceleration.x = lin_acc_x[COV_MATRIX_SAMPLE - 1];
    imu_state.linear_acceleration.y = lin_acc_y[COV_MATRIX_SAMPLE - 1];
    imu_state.linear_acceleration.z = lin_acc_z[COV_MATRIX_SAMPLE - 1];


    imu_state.linear_acceleration_covariance[0] = covArrays(lin_acc_x, lin_acc_x);
    imu_state.linear_acceleration_covariance[1] = covArrays(lin_acc_x, lin_acc_y);
    imu_state.linear_acceleration_covariance[2] = covArrays(lin_acc_x, lin_acc_z);
    imu_state.linear_acceleration_covariance[3] = imu_state.linear_acceleration_covariance[1];
    imu_state.linear_acceleration_covariance[4] = covArrays(lin_acc_y, lin_acc_y);
    imu_state.linear_acceleration_covariance[5] = covArrays(lin_acc_y, lin_acc_z);
    imu_state.linear_acceleration_covariance[6] = imu_state.linear_acceleration_covariance[2];
    imu_state.linear_acceleration_covariance[7] = imu_state.linear_acceleration_covariance[5];
    imu_state.linear_acceleration_covariance[8] = covArrays(lin_acc_z, lin_acc_z);

    */
}














/***************************************************************
 * **************** PUBLISH JOINT STATE FUNCTION ***************
 ***************************************************************/

void sendRobotData(){
    if(micros() - TimerPublisher < 1000000/PUBLISH_DATA_FREQ){
      return;
    }

    /*********** JOINTS STATE *************/
    float position[NUM_MOTORS];
    float velocity[NUM_MOTORS];
    float effort[NUM_MOTORS];

    for(short i=0; i < NUM_MOTORS; i++){
      position[i] = servomotors[i]->getPosition();
      velocity[i] = servomotors[i]->getVelocity();
      effort[i]   = servomotors[i]->getTorque();

    }
    joint_state.position   = position;
    joint_state.velocity   = velocity;
    joint_state.effort     = effort;

    joint_state.header.stamp = nh.now();
    jointStatePub.publish( &joint_state );

    /************ IMU STATE ***********/
    imu_state.header.stamp = nh.now();
    imu_state.header.frame_id = "base_link";
    IMUStatePub.publish( &imu_state );
    
    TimerPublisher = micros();    
}








/***************************************************************
 * **************** PUBLISH FREQ FUNCTION **********************
 ***************************************************************/

 
void publishInfoFreq(){
    unsigned long deltaT = micros()  - TimerFreqPubliser;
    if(deltaT < 1000000/PUBLISH_INFO_FREQ){
      return;
    }

    double freq = double(deltaT)/double(counts_loop);
    freq = 1000000/freq;

    loop_freq.data = freq;
    freqInfoPub.publish( &loop_freq );


    TimerFreqPubliser = micros();
    counts_loop = 0;
}








/***************************************************************
 * **************** LOOP FUNCTION *****************************
 ***************************************************************/

void loop(){
  double dt = float(micros() - Timer) / 1000000.0;
  Timer = micros();
  for(short i=0; i < NUM_MOTORS; i++){
      servomotors[i]->run(dt);
  }
  getImuData();


  sendRobotData();
  nh.spinOnce();

  publishInfoFreq();
  counts_loop += 1;
}
