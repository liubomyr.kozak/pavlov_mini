#!/usr/bin/env python
import rospy
#!/usr/bin/env python
import rospy
import rospkg
import sys

rospack = rospkg.RosPack()
#sys.path.append(rospack.get_path('pavlov_drivers') + '/src/')


from visualization_msgs.msg import Marker
from geometry_msgs.msg import Pose, Point, Vector3, Quaternion, PolygonStamped, Point32
from sensor_msgs.msg import JointState
import tf
from tf.transformations import euler_from_quaternion, quaternion_from_euler
import numpy as np
from  pavlov_mini_msgs.msg import feet_pressure, robot_state
import numpy as np
import numpy as np
from sensor_msgs.msg import JointState



def transformPoint(pos, trans, quaternion):
    trans_mat = tf.transformations.translation_matrix(trans)
    rot_mat = tf.transformations.quaternion_matrix(quaternion)
    tfMatrix = np.dot(rot_mat, trans_mat)


class PavlovMiniVisualization:
    def __init__(self):
	self.debug("INFO", "pavlov_mini_visualization_node is running!")

        ####### publisher ##########
        #self.pub = rospy.Publisher('chatter', String, queue_size=10)

        self.feetPressPub     = rospy.Publisher('/pavlov_mini/visualization/feet_pressure', Marker, queue_size=10)
        self.feetPressCntrPub = rospy.Publisher('/pavlov_mini/visualization/feet_pressure_control', Marker,  queue_size=10)

        self.robotStateMarkerPub    = rospy.Publisher('/pavlov_mini/visualization/robot_state', Marker, queue_size=10)
        self.robotStateMarkerPub = rospy.Publisher('/pavlov_mini/visualization/joints_torques',  Marker, queue_size=10)
        self.joint_state = JointState()

        ####### subscribers ########
        rospy.Subscriber('/pavlov_mini/feet_pressure',         feet_pressure, self.feetPressCB)
        rospy.Subscriber('/pavlov_mini/feet_pressure_control', feet_pressure, self.feetPressCntCB)
        rospy.Subscriber('/pavlov_mini/state_estimation',     robot_state, self.robotStateCB)

        #rospy.Subscriber('/pavlov_mini/imu_data',    Imu,           self.imuDataCallBack)
        #rospy.Subscriber('/pavlov_mini/zmp_state',    zmp,          self.zmpStateCallBack)

        self.tfListener = tf.TransformListener()

        self.curr_robot_state    = robot_state()
        self.curr_feet_press     = feet_pressure()
        self.curr_feet_press_cnt = feet_pressure()



    """ call backs functions """
    def feetPressCB(self, data):
        self.curr_feet_press = data


    def feetPressCntCB(self, data):
        self.curr_feet_press_cnt = data


    def robotStateCB(self, data):
        self.curr_robot_state = data




    def publishFeetPressure(self, data, color, publisher):
        # make a visualization marker array for the occupancy grid

        for i in range(len(data.leg_name)):
            name     = data.leg_name[i]
            pressure = data.pressure_vector[i]

            try:
                (trans, rot) = self.tfListener.lookupTransform('world', 'feet_' + name, rospy.Time(0))

            except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                print('cannot compute transform for feet_' + name)
                continue



            m = Marker()
            m.action = Marker.ADD
            m.header.frame_id = 'world'
            m.header.stamp = rospy.Time.now()
            m.ns = 'pressure_{:s}'.format( name )
            m.id = 0
            m.type = Marker.LINE_STRIP


            startPoint = Point()
            startPoint.x = trans[0]
            startPoint.y = trans[1]
            startPoint.z = trans[2]

            endPoints = Point()
            endPoints.x = trans[0] + 10*pressure.x
            endPoints.y = trans[1] + 10*pressure.y
            endPoints.z = trans[2] + 10*pressure.z



            m.points.append(startPoint)
            m.points.append(endPoints)



            m.scale.x = 0.01
            m.scale.y = 0.01
            m.scale.z = 0.01

            m.color.r = color[0]
            m.color.g = color[1]
            m.color.b = color[2]
            m.color.a = color[3]


            publisher.publish(m)






    def publishRobotState(self, color):
        # make a visualization marker array for the occupancy grid

        m = Marker()
        m.action = Marker.ADD
        m.header.frame_id = 'ground'
        m.header.stamp = rospy.Time.now()
        m.ns = 'base_velocity'
        m.id = 0
        m.type = Marker.LINE_STRIP

        startPoint = Point()
        startPoint.x = self.curretRobotState.pose.position.x
        startPoint.y = self.curretRobotState.pose.position.y
        startPoint.z = self.curretRobotState.pose.position.z

        endPoints = Point()
        endPoints.x = startPoint.x + 1*self.curretRobotState.linear_velocity.x
        endPoints.y = startPoint.y + 1*self.curretRobotState.linear_velocity.y
        endPoints.z = startPoint.z + 1*self.curretRobotState.linear_velocity.z

        m.points.append(startPoint)
        m.points.append(endPoints)

        m.scale.x = 0.01
        m.scale.y = 0.01
        m.scale.z = 0.01

        m.color.r = color[0]
        m.color.g = color[1]
        m.color.b = color[2]
        m.color.a = color[3]

        self.robotStateMarkerPub.publish(m)

    '''
    def publishCoM(self):
        # make a visualization marker array for the occupancy grid


        m = Marker()
        m.action = Marker.ADD
        m.header.frame_id = 'ground'
        m.header.stamp = rospy.Time.now()
        m.ns = 'ZMP'
        m.id = 0
        m.type = Marker.SPHERE

        m.pose.position.x = self.currentZMPstate.zmp_pos.x
        m.pose.position.y = self.currentZMPstate.zmp_pos.y
        m.pose.position.z = self.currentZMPstate.zmp_pos.z

        m.pose.orientation.w = 0
        m.pose.orientation.x = 0
        m.pose.orientation.y = 0
        m.pose.orientation.z = 1

        m.scale.x = 0.02
        m.scale.y = 0.02
        m.scale.z = 0.02

        m.color.r = 0
        m.color.g = 1
        m.color.b = 0
        m.color.a = 1
        self.CoMgroundPub.publish(m)


    




    def publishStableRegion(self):


        polygon = PolygonStamped()

        for p in self.currentZMPstate.stable_region.points:
            polygon.polygon.points.append(p)


        polygon.header.stamp = rospy.Time.now()
        polygon.header.frame_id = 'ground'
        self.stableRegPub.publish(polygon)


    def publishGravity(self):
        try:
            (trans, rot) = self.tfListener.lookupTransform('ground', 'torso', rospy.Time(0))

        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            print('cannot compute transform for gravity')
            return

        m = Marker()
        m.action = Marker.ADD
        m.header.frame_id = 'ground'
        m.header.stamp = rospy.Time.now()
        m.ns = 'gravity'
        m.id = 0
        m.type = Marker.LINE_STRIP

        pStart = Point()
        pStart.x = trans[0]
        pStart.y = trans[1]
        pStart.z = trans[2]
        m.points.append(pStart)

        pEnd = Point()

        pEnd.x = self.currentZMPstate.zmp_pos.x
        pEnd.y = self.currentZMPstate.zmp_pos.y
        pEnd.z = self.currentZMPstate.zmp_pos.z

        #pEnd.x = trans[0] - self.imu_data.linear_acceleration.x*0.04
        #pEnd.y = trans[1] - self.imu_data.linear_acceleration.y*0.04
        #pEnd.z = trans[2] - self.imu_data.linear_acceleration.z*0.04

        m.points.append(pEnd)

        m.scale.x = 0.005
        m.scale.y = 0.005
        m.scale.z = 0.005

        m.color.r = 0.75
        m.color.g = 0.75
        m.color.b = 0.75
        m.color.a = 1

        self.CoMgroundPub.publish(m)

    '''

    """ Code for the main thread of the node """
    def mainThread(self):

        self.publishFeetPressure(self.curr_feet_press,[0, 1, 0, 1],self.feetPressPub)
        self.publishFeetPressure(self.curr_feet_press_cnt,[1, 0, 0, 1],self.feetPressCntrPub)

        #self.publishRobotState([1,0,0,1])
        #self.publishCoM()
        #self.publishStableRegion()
        #self.publishGravity()

    def debug(self, typ, msg):
        print typ + ": " + msg + "\n"


if __name__ == '__main__':
    try:
        rospy.init_node('pavlov_mini_visualization_node', anonymous=True)
        rate = rospy.Rate(30)    # 10 Hz
        node = PavlovMiniVisualization()

        while not rospy.is_shutdown():
            node.mainThread()
            rate.sleep()

    except rospy.ROSInterruptException:
        pass

