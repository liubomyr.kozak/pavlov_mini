#!/usr/bin/env python
import rospy
from std_msgs.msg import String
import pygame
from geometry_msgs.msg import TwistStamped, PoseStamped
import numpy as np
import pygame
from tf import transformations

class ManualControlNode:
    def __init__(self):
	self.debug("INFO", "manual control_node is running!")
        pygame.init()

        pygame.font.init()  # you have to call this at the start,
        textFont = pygame.font.SysFont('Comic Sans MS', 30)
        self.screen = pygame.display.set_mode((640, 480))
        self.screen.fill((0, 0, 0))
        text = []
        text.append(textFont.render('Use the keys "w", "s", "a", "d" to move', True, (255, 255, 255)) )
        text.append(textFont.render('forward, backwards, left or right and', True, (255, 255, 255)) )
        text.append(textFont.render('      "q" and "e" to move yaw', True, (255, 255, 255)) )
        for i in range(len(text)):
            self.screen.blit(text[i], (100, 200 + i*30))
        pygame.display.update()

        self.keys = [pygame.K_w, pygame.K_s,
                     pygame.K_q, pygame.K_e,
                     pygame.K_a, pygame.K_d,
                     pygame.K_r, pygame.K_y,
                     pygame.K_f, pygame.K_h,
                     pygame.K_t, pygame.K_g,
                     pygame.K_u, pygame.K_o,
                     pygame.K_j, pygame.K_l,
                     pygame.K_i, pygame.K_k,
                     pygame.K_u, pygame.K_j]

        self.pressed_key = [False] * len(self.keys)


        self.torsoCmd = PoseStamped()
        self.torsoCmd.pose.orientation.w = 1
        self.torsoCmd.pose.position.z = 0.15

        ####### publisher ##########
        self.velPub   = rospy.Publisher("/pavlov_mini/vel_cmd", TwistStamped, queue_size=10)
        self.torsoPub = rospy.Publisher("/pavlov_mini/torso_pose_control", PoseStamped, queue_size=10)

    """ Code for the main thread of the node """
    def mainThread(self):
        for event in pygame.event.get():
            for i in range(len(self.keys)):
                if event.type == pygame.KEYDOWN:  # check for key presses
                    if event.key == self.keys[i]:
                        self.pressed_key[i] = True

                elif event.type == pygame.KEYUP:  # check for key presses
                    if event.key == self.keys[i]:
                        self.pressed_key[i] = False

        velCmd = TwistStamped()
        velCmd.header.stamp = rospy.Time.now()
        velCmd.twist.linear.x = 0
        velCmd.twist.linear.y = 0
        velCmd.twist.linear.z = 0
        velCmd.twist.angular.z = 0

        velxy = 0.5  # m/s
        velz  = 0.03  # m/s
        velth = 10 #0.3*(np.pi/180)  # rad/s


        if self.pressed_key[0]: # w
            velCmd.twist.linear.x = velxy

        if self.pressed_key[1]: #s
            velCmd.twist.linear.x = -velxy
        if self.pressed_key[2]: #a
            velCmd.twist.linear.y = velxy
        if self.pressed_key[3]: #d
            velCmd.twist.linear.y = -velxy

        if self.pressed_key[4]: #q
            velCmd.twist.angular.z = - velth
        if self.pressed_key[5]: #e
            velCmd.twist.angular.z = velth


        vel_pos_body = 0.0001
        vel_ang_body = 0.001

        if self.pressed_key[6]: #r
            velCmd.twist.angular.x += velth
        if self.pressed_key[7]: #y
            velCmd.twist.angular.x += -velth
        if self.pressed_key[8]: #f
            velCmd.twist.angular.y += velth
        if self.pressed_key[9]: #h
            velCmd.twist.angular.y += -velth


        if self.pressed_key[10]: #t
            velCmd.twist.angular.z += velxy
        if self.pressed_key[11]: #g
            velCmd.twist.angular.y += -velxy

        if self.pressed_key[18]: #u
            velCmd.twist.linear.z += velz
        if self.pressed_key[19]: #j
            velCmd.twist.linear.z += - velz


        self.velPub.publish(velCmd)

        #print(velCmd.twist.linear.x, velCmd.twist.linear.y, velCmd.twist.angular.z)

    def debug(self, typ, msg):
        print typ + ": " + msg + "\n"


if __name__ == '__main__':
    try:
        rospy.init_node('manual_control_node', anonymous=True)
        rate = rospy.Rate(10)    # 10 Hz
        node = ManualControlNode()

        while not rospy.is_shutdown():
            node.mainThread()
            rate.sleep()

    except rospy.ROSInterruptException:
        pass

