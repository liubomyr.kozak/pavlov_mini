//
// Created by andres on 07.11.20.
//


#include "../include/state_estimation.h"


void crossProduct(const Eigen::Matrix<float, 3, 1> &v1, const Eigen::Matrix<float, 3, 1> &v2, Eigen::Matrix<float, 3, 1>* result){
    Eigen::Matrix<float, 3, 1> r;

    r << v1(1,0)*v2(2,0)- v1(2,0)*v2(1,0),   -v1(0,0)*v2(2,0) + v1(2,0)*v2(0,0),   v1(0,0)*v2(1,0)- v1(1,0)*v2(0,0);
    result->operator=( r );
}

StateEstimation::StateEstimation(Kinematics* kinematics){
    this->kinematics = kinematics;
    this->reset();
}

StateEstimation::~StateEstimation(){

}

void StateEstimation::reset() {
    this->x.setZero();
    this->dt = 0;
    this->P.fill(0.1);


    bool feet_stance[4] = {0,0,0,0};
    float joint_pos[12] = {0,0,0,0,0,0,0,0,0,0,0,0};
    this->feet_stance = feet_stance;
    this->joint_angles = joint_pos;

    this->O.setZero();

    this->I << 1,0,0,
               0,1,0,
               0,0,1;
    this->base_quaternion << 1,0,0,0;
    this->angular_vel     << 0,0,0;
    this->acc             << 0,0,0;

    this->base_rotation_matrix = this->I;

    Eigen::Matrix<float, 1, 3> vz, vO;
    vz << 0,0,-1;
    vO << 0,0, 0;


    Eigen::Matrix<float, 3, STATE_DIM> H0, H1, H2, H3, H4, H5, H6, H7;
    Eigen::Matrix<float, 1, STATE_DIM> H8, H9, H10, H11;

    H0  <<  I,  O, -I,  O,  O,  O;
    H1  <<  I,  O,  O, -I,  O,  O;
    H2  <<  I,  O,  O,  O, -I,  O;
    H3  <<  I,  O,  O,  O,  O, -I;
    H4  <<  O, -I,  O,  O,  O,  O;
    H5  <<  O, -I,  O,  O,  O,  O;
    H6  <<  O, -I,  O,  O,  O,  O;
    H7  <<  O, -I,  O,  O,  O,  O;
    H8  << vO, vO, vz, vO, vO, vO;
    H9  << vO, vO, vO, vz, vO, vO;
    H10 << vO, vO, vO, vO, vz, vO;
    H11 << vO, vO, vO, vO, vO, vz;



    this->H << H0, H1, H2, H3, H4, H5, H6, H7, H8, H9, H10, H11;


    Eigen::Matrix<float, 3, STATE_DIM> Idim_0, Idim_1, Idim_2, Idim_3, Idim_4, Idim_5;
    Idim_0 << I, O, O, O, O, O;
    Idim_1 << O, I, O, O, O, O;
    Idim_2 << O, O, I, O, O, O;
    Idim_3 << O, O, O, I, O, O;
    Idim_4 << O, O, O, O, I, O;
    Idim_5 << O, O, O, O, O, I;

    this->I_state_dim << Idim_0, Idim_1, Idim_2, Idim_3, Idim_4, Idim_5;


}

void StateEstimation::setInitialState(const Eigen::Vector4f &base_quaternion_, float *jointPos) {
    this->base_quaternion.operator=(base_quaternion_);

    float s_ = this->base_quaternion.w();
    float x_ = this->base_quaternion.x();
    float y_ = this->base_quaternion.y();
    float z_ = this->base_quaternion.z();


    this->base_rotation_matrix << 1 - 2*y_*y_ - 2*z_*z_,  2*x_*y_ - 2*s_*z_,       2*x_*z_ + 2*s_*y_,
                                  2*x_*y_ + 2*s_*z_,      1 - 2*x_*x_ - 2*z_*z_,   2*y_*z_ - 2*s_*x_,
                                  2*x_*z_ - 2*s_*y_,      2*y_*z_ + 2*s_*x_,       1 - 2*x_*x_ - 2*y_*y_;

    this->joint_angles = jointPos;

    float pos_0[3] = {jointPos[0], jointPos[1], jointPos[2]};
    float pos_1[3] = {jointPos[3], jointPos[4], jointPos[5]};
    float pos_2[3] = {jointPos[6], jointPos[7], jointPos[8]};
    float pos_3[3] = {jointPos[9], jointPos[10], jointPos[11]};


    Eigen::Matrix<float, 3, 1> pos_rel_feet_fl, pos_rel_feet_fr, pos_rel_feet_bl, pos_rel_feet_br;
    pos_rel_feet_fl << 0,0,0; //getFeetPos(0, pos_0);
    pos_rel_feet_fr << 0,0,0; //getFeetPos(0, pos_0);
    pos_rel_feet_bl << 0,0,0; //getFeetPos(0, pos_0);
    pos_rel_feet_br << 0,0,0; //getFeetPos(0, pos_0);

    float mean_z = (pos_rel_feet_fl(2,0) + pos_rel_feet_fr(2,0) + pos_rel_feet_bl(2,0) + pos_rel_feet_br(2,0) )/4;

    Eigen::Matrix<float, 3, 1> base_pos;
    base_pos << 0,0, -mean_z;

    Eigen::Matrix<float, 3, 1> pos_feet_fl, pos_feet_fr, pos_feet_bl, pos_feet_br;
    pos_feet_fl = base_pos + pos_rel_feet_fl;
    pos_feet_fr = base_pos + pos_rel_feet_fr;
    pos_feet_bl = base_pos + pos_rel_feet_bl;
    pos_feet_br = base_pos + pos_rel_feet_br;


    Eigen::Matrix<float, 3, 1> base_vel;
    base_vel << 0,0, 0;


    this->x << base_pos  ,
               base_vel   ,
               pos_feet_fl,
               pos_feet_fr,
               pos_feet_bl,
               pos_feet_br;

}

void StateEstimation::getFeetPositionCovariance(bool feet_stance, Eigen::Matrix3f* Qfeet){
    float w = pow(STD_FEET_SWING, 2);
    if (feet_stance){
        w = pow(STD_FEET_STANCE, 2);
    }
    Eigen::Matrix3f Q_;
    Q_ << w, 0, 0,
         0, w, 0,
         0, 0, w;

    Qfeet->operator=(Q_);
}


float StateEstimation::getFeetHeightCovariance(bool feet_stance){
    float w = pow(STD_FEET_HEIGHT_SWING, 2);
    if (feet_stance){
        w = pow(STD_FEET_HEIGHT_STANCE, 2);
    }
    return w;
}



void StateEstimation::setInput(const Eigen::Vector4f &base_quaternion_, const Eigen::Vector3f &acc_, bool *feet_stance, float deltat) {

    this->base_quaternion.operator=(base_quaternion_);
    this->feet_stance = feet_stance;
    float s_ = this->base_quaternion.w();
    float x_ = this->base_quaternion.x();
    float y_ = this->base_quaternion.y();
    float z_ = this->base_quaternion.z();

    this->base_rotation_matrix << 1 - 2*y_*y_ - 2*z_*z_,  2*x_*y_ - 2*s_*z_,       2*x_*z_ + 2*s_*y_,
                                  2*x_*y_ + 2*s_*z_,      1 - 2*x_*x_ - 2*z_*z_,   2*y_*z_ - 2*s_*x_,
                                  2*x_*z_ - 2*s_*y_,      2*y_*z_ + 2*s_*x_,       1 - 2*x_*x_ - 2*y_*y_;


    Eigen::Matrix<float, 3, 1> acc_tmp;
    acc_tmp << acc_.x(), acc_.y(), acc_.z();

    this->acc = this->base_rotation_matrix * acc_tmp;
    this->dt = deltat;

    Eigen::Matrix3f Q_acc;
    Q_acc <<  pow(STD_LIN_ACC_X,2), 0, 0,
              0, pow(STD_LIN_ACC_Y,2), 0,
              0, 0, pow(STD_LIN_ACC_Z,2);


    Eigen::Matrix3f Qfl_tf, Qfr_tf, Qbl_tf, Qbr_tf;
    this->getFeetPositionCovariance(feet_stance[0], &Qfl_tf);
    this->getFeetPositionCovariance(feet_stance[1], &Qfr_tf);
    this->getFeetPositionCovariance(feet_stance[2], &Qbl_tf);
    this->getFeetPositionCovariance(feet_stance[3], &Qbr_tf);

    Eigen::Matrix3f Qfl, Qfr, Qbl, Qbr;
    Qfl = this->base_rotation_matrix.transpose() * Qfl_tf * this->base_rotation_matrix;
    Qfr = this->base_rotation_matrix.transpose() * Qfr_tf * this->base_rotation_matrix;
    Qbl = this->base_rotation_matrix.transpose() * Qbl_tf * this->base_rotation_matrix;
    Qbr = this->base_rotation_matrix.transpose() * Qbr_tf * this->base_rotation_matrix;

    Eigen::Matrix3f Qacc1, Qacc2, Qacc3;

    Qacc1  <<  dt*Q_acc(0,0), dt*Q_acc(0,1), dt*Q_acc(0,2),
               dt*Q_acc(1,0), dt*Q_acc(1,1), dt*Q_acc(1,2),
               dt*Q_acc(2,0), dt*Q_acc(2,1), dt*Q_acc(2,2);

    float dt2 = pow(dt,2)/2;
    Qacc2 << dt2*Q_acc(0,0), dt2*Q_acc(0,1), dt2*Q_acc(0,2),
             dt2*Q_acc(1,0), dt2*Q_acc(1,1), dt2*Q_acc(1,2),
             dt2*Q_acc(2,0), dt2*Q_acc(2,1), dt2*Q_acc(2,2);

    float dt3 = pow(dt,3)/3;
    Qacc3 << dt3*Q_acc(0,0), dt3*Q_acc(0,1), dt3*Q_acc(0,2),
             dt3*Q_acc(1,0), dt3*Q_acc(1,1), dt3*Q_acc(1,2),
             dt3*Q_acc(2,0), dt3*Q_acc(2,1), dt3*Q_acc(2,2);


    Eigen::Matrix<float, 3, STATE_DIM> Q0, Q1, Q2, Q3, Q4, Q5;

    Q0 << Qacc3, Qacc2, O, O, O, O;
    Q1 << Qacc2, Qacc1, O, O, O, O;
    Q2 << O, O, Qfl, O, O, O;
    Q3 << O, O, O, Qfr, O, O;
    Q4 << O, O, O, O, Qbl, O;
    Q5 << O, O, O, O, O, Qbr;

    this->Q << Q0, Q1, Q2, Q3, Q4, Q5;
}


void StateEstimation::predictState() {
    Eigen::Matrix3f Idt, Idt2;
    float dt2 = pow(dt,2)/2;

    Idt << dt, 0,  0,
           0 , dt, 0,
           0,  0,  dt;

    Idt2 << dt2, 0,     0,
            0,    dt2,  0,
            0,    0,    dt2;

    Eigen::Matrix<float, 3, STATE_DIM> A0, A1, A2, A3, A4, A5;
    A0 << I, Idt, O, O, O, O;
    A1 << O, I, O, O, O, O;
    A2 << O, O, I, O, O, O;
    A3 << O, O, O, I, O, O;
    A4 << O, O, O, O, I, O;
    A5 << O, O, O, O, O, I;

    Eigen::Matrix<float, STATE_DIM, STATE_DIM> A;
    A << A0, A1, A2, A3, A4, A5;

    Eigen::Matrix<float, STATE_DIM, 3> B;
    B << Idt2,
         Idt,
         O,
         O,
         O,
         O;

    this->x = A * this->x + B * this->acc;
    this->P = A* this->P * A.transpose() + this->Q;
}


void StateEstimation::getFeetFromBaseRotated(std::string leg, const Eigen::Vector3f joint_angles, Eigen::Matrix<float, 3, 1>* feet_pos_from_base_rotated){
    Eigen::Vector3f feet_from_base;
    this->kinematics->feetPosFromBase(leg, joint_angles, &feet_from_base);

    Eigen::Matrix<float, 3, 1> feet_from_base_col;

    feet_from_base_col << feet_from_base.x(), feet_from_base.y(), feet_from_base.z();

    feet_pos_from_base_rotated->operator=( this->base_rotation_matrix * feet_from_base_col );

}


void StateEstimation::setMeasurement(const Eigen::Vector3f gyro, float *jointPos, float *jointVel, float *feetHeight_) {
    this->angular_vel = this->base_rotation_matrix * (-gyro);

    Eigen::Vector3f angles_fl, angles_fr, angles_bl, angles_br;
    angles_fl << jointPos[0], jointPos[1], jointPos[2];
    angles_fr << jointPos[3], jointPos[4], jointPos[5];
    angles_bl << jointPos[6], jointPos[7], jointPos[8];
    angles_br << jointPos[9], jointPos[10], jointPos[11];

    Eigen::Matrix<float, 3, 1> feet_rel_pos_fl, feet_rel_pos_fr, feet_rel_pos_bl, feet_rel_pos_br;
    this->getFeetFromBaseRotated("fl", angles_fl, &feet_rel_pos_fl);
    this->getFeetFromBaseRotated("fr", angles_fr, &feet_rel_pos_fr);
    this->getFeetFromBaseRotated("bl", angles_bl, &feet_rel_pos_bl);
    this->getFeetFromBaseRotated("br", angles_br, &feet_rel_pos_br);





    //std::cout << "relative fl pos " << feet_rel_pos_fl.x() << " "  << feet_rel_pos_fl.y() << " "  << feet_rel_pos_fl.z() << std::endl;
    //std::cout << "relative fr pos " << feet_rel_pos_fr.x() << " "  << feet_rel_pos_fr.y() << " "  << feet_rel_pos_fr.z() << std::endl;
    //std::cout << "relative bl pos " << feet_rel_pos_bl.x() << " "  << feet_rel_pos_bl.y() << " "  << feet_rel_pos_bl.z() << std::endl;
    //std::cout << "relative br pos " << feet_rel_pos_br.x() << " "  << feet_rel_pos_br.y() << " "  << feet_rel_pos_br.z() << std::endl;



    Eigen::Vector3f ang_vel_fl, ang_vel_fr, ang_vel_bl, ang_vel_br;
    ang_vel_fl << jointVel[0], jointVel[1], jointVel[2];
    ang_vel_fr << jointVel[3], jointVel[4], jointVel[5];
    ang_vel_bl << jointVel[6], jointVel[7], jointVel[8];
    ang_vel_br << jointVel[9], jointVel[10], jointVel[11];



    Eigen::Matrix3f J_fl, J_fr, J_bl, J_br;
    this->kinematics->getJacobian("fl", angles_fl, &J_fl);
    this->kinematics->getJacobian("fr", angles_fr, &J_fr);
    this->kinematics->getJacobian("bl", angles_bl, &J_bl);
    this->kinematics->getJacobian("br", angles_br, &J_br);

    Eigen::Matrix<float, 3, 1> cross_fl, cross_fr, cross_bl, cross_br;
    crossProduct(this->angular_vel,  feet_rel_pos_fl,   &cross_fl);
    crossProduct(this->angular_vel,  feet_rel_pos_fr,   &cross_fr);
    crossProduct(this->angular_vel,  feet_rel_pos_bl,   &cross_bl);
    crossProduct(this->angular_vel,  feet_rel_pos_br,   &cross_br);

    Eigen::Matrix<float, 3, 1> feet_rel_vel_fl, feet_rel_vel_fr, feet_rel_vel_bl, feet_rel_vel_br;
    feet_rel_vel_fl  = cross_fl + this->base_rotation_matrix * J_fl * ang_vel_fl;
    feet_rel_vel_fr  = cross_fr + this->base_rotation_matrix * J_fr * ang_vel_fr;
    feet_rel_vel_bl  = cross_bl + this->base_rotation_matrix * J_bl * ang_vel_bl;
    feet_rel_vel_br  = cross_br + this->base_rotation_matrix * J_br * ang_vel_br;


    //feet_rel_vel_fl << 0,0,0;
    //feet_rel_vel_fr << 0,0,0;
    //feet_rel_vel_bl << 0,0,0;
    //feet_rel_vel_br << 0,0,0;

    //std::cout << "relative fl vel " << feet_rel_vel_fl.x() << " "  << feet_rel_vel_fl.y() << " "  << feet_rel_vel_fl.z() << std::endl;
    //std::cout << "relative fr vel " << feet_rel_vel_fr.x() << " "  << feet_rel_vel_fr.y() << " "  << feet_rel_vel_fr.z() << std::endl;
    //std::cout << "relative bl vel " << feet_rel_vel_bl.x() << " "  << feet_rel_vel_bl.y() << " "  << feet_rel_vel_bl.z() << std::endl;
    //std::cout << "relative br vel " << feet_rel_vel_br.x() << " "  << feet_rel_vel_br.y() << " "  << feet_rel_vel_br.z() << std::endl;


    Eigen::Matrix<float, 4, 1> feetHeight;
    feetHeight << feetHeight_[0], feetHeight_[1], feetHeight_[2], feetHeight_[3];




    this->z << -feet_rel_pos_fl,
               -feet_rel_pos_fr,
               -feet_rel_pos_bl,
               -feet_rel_pos_br,
               -feet_rel_vel_fl,
               -feet_rel_vel_fr,
               -feet_rel_vel_bl,
               -feet_rel_vel_br,
               -feetHeight;


    Eigen::Matrix3f Qpos, Qvel;
    Qpos << pow(STD_FEET_POS_REL,2), 0, 0,
            0, pow(STD_FEET_POS_REL,2), 0,
            0, 0, pow(STD_FEET_POS_REL,2);

    Qvel << pow(STD_FEET_VEL_REL,2), 0, 0,
            0, pow(STD_FEET_VEL_REL,2), 0,
            0, 0, pow(STD_FEET_VEL_REL,2);

    Eigen::Matrix4f Qheight;
    Qheight << getFeetHeightCovariance(this->feet_stance[0]), 0, 0, 0,
               0, getFeetHeightCovariance(this->feet_stance[1]), 0, 0,
               0, 0, getFeetHeightCovariance(this->feet_stance[2]), 0,
               0, 0, 0, getFeetHeightCovariance(this->feet_stance[3]);

    Eigen::Matrix<float, 3, 4> c;
    Eigen::Matrix<float, 4, 3> cT;
    c.fill(0);
    cT.fill(0);

    Eigen::Matrix<float, 3, MEAS_DIM> R0, R1, R2, R3, R4, R5, R6, R7;
    Eigen::Matrix<float, 4, MEAS_DIM> R8;
    R0 <<       Qpos , O    , O    , O    , O    , O    , O    , O    , c;
    R1 <<       O    , Qpos , O    , O    , O    , O    , O    , O    , c;
    R2 <<       O    , O    , Qpos , O    , O    , O    , O    , O    , c;
    R3 <<       O    , O    , O    , Qpos , O    , O    , O    , O    , c;
    R4 <<       O    , O    , O    , O    , Qvel , O    , O    , O    , c;
    R5 <<       O    , O    , O    , O    , O    , Qvel , O    , O    , c;
    R6 <<       O    , O    , O    , O    , O    , O    , Qvel , O    , c;
    R7 <<       O    , O    , O    , O    , O    , O    , O    , Qvel , c;
    R8 <<       cT   , cT   , cT   , cT   , cT   , cT   , cT   , cT   , Qheight;


    this->R << R0, R1, R2, R3, R4, R5, R6, R7, R8;

}

void StateEstimation::updateState() {
    Eigen::Matrix<float, MEAS_DIM, 1> y;
    y = this->z - this->H * this->x;

     /*
    std::cout << "P:" << std::endl;
    for (int i=0; i < STATE_DIM; i++){
        for (int j=0; j < STATE_DIM; j++){
            std::cout << this->P(i,j) << ", ";
        }
        std::cout << std::endl;
    }
    */

    /*
    std::cout << "R:" << std::endl;
    for (int i=0; i < MEAS_DIM; i++){
        for (int j=0; j < MEAS_DIM; j++){
            std::cout << this->R(i,j) << ", ";
        }
        std::cout << std::endl;
    }
    */



    Eigen::Matrix<float, MEAS_DIM, MEAS_DIM> inv_;
    inv_ = this->H * this->P * (this->H.transpose()) + this->R;

    /*
    std::cout << "before inversion:" << std::endl;
    for (int i=0; i < MEAS_DIM; i++){
        for (int j=0; j < MEAS_DIM; j++){
            std::cout << inv_(i,j) << ", ";
        }
        std::cout << std::endl;
    }
    */


    // HERE IT BECOMES NAN BECAUSE THE inv_ MATRIX IS SOMEHOW WRONG!
    inv_ = inv_.inverse();

    /*
    std::cout << "inverse:" << std::endl;
    for (int i=0; i < MEAS_DIM; i++){
        for (int j=0; j < MEAS_DIM; j++){
            std::cout << inv_(i,j) << ", ";
    //        if (isnan(inv_(i,j))){
    //            return;
    //        }
        }
        std::cout << std::endl;
    }
    */


    Eigen::Matrix<float, STATE_DIM, MEAS_DIM> Kalman_gain;



    Kalman_gain = this->P * (this->H.transpose()) * inv_;
    this->x = this->x + Kalman_gain * y;
    this->P = (this->I_state_dim - Kalman_gain * this->H)*this->P;

}



void StateEstimation::getBasePosition(Eigen::Vector3f* base_pos){
    Eigen::Vector3f base_pos_;
    base_pos_ << this->x(0,0), this->x(1,0), this->x(2,0);
    base_pos->operator=(base_pos_);
}

void StateEstimation::getBaseVelocity(Eigen::Vector3f* base_vel){
    Eigen::Vector3f base_vel_;
    base_vel_ << this->x(3,0), this->x(4,0), this->x(5,0);
    base_vel->operator=(base_vel_);
}


void StateEstimation::getBaseAngularVelocity(Eigen::Vector3f* base_angular_vel){
    base_angular_vel->operator=(  this->angular_vel  );
}


void StateEstimation::getFeetPosition(std::string leg, Eigen::Vector3f* feet_pos){
    Eigen::Vector3f feet_pos_;
    if (leg == "fl"){
        feet_pos_ << this->x(6,0), this->x(7,0), this->x(8,0);
    }
    else if (leg == "fr"){
        feet_pos_ << this->x(9,0), this->x(10,0), this->x(11,0);
    }
    else if (leg == "bl"){
        feet_pos_ << this->x(12,0), this->x(13,0), this->x(14,0);
    }
    else if (leg == "br"){
        feet_pos_ << this->x(15,0), this->x(16,0), this->x(17,0);
    }

    feet_pos->operator=( feet_pos_ );
}


void StateEstimation::getBaseVelocityFromBaseRef(Eigen::Vector3f* base_vel){
    Eigen::Vector3f base_vel_;
    base_vel_ << this->x(3,0), this->x(4,0), this->x(5,0);
    base_vel->operator=(this->base_rotation_matrix.transpose() * base_vel_);
}



void StateEstimation::getBaseAngularVelocityFromBaseRef(Eigen::Vector3f* base_ang_vel){
    Eigen::Vector3f base_vel_;
    base_ang_vel->operator=(this->base_rotation_matrix.transpose() * this->angular_vel);
}

void StateEstimation::getBaseRotationMatrix(Eigen::Matrix3f* base_rot_matrix){
    base_rot_matrix->operator=( this->base_rotation_matrix  );
}


void StateEstimation::getBaseAccelerationFromBaseRef(Eigen::Vector3f* base_acc){
    base_acc->operator=(this->acc);
}

float StateEstimation::getMeanFeetHeight(){
    float feet_fl_z, feet_fr_z, feet_bl_z, feet_br_z;
    feet_fl_z = this->x(8,0);
    feet_fr_z = this->x(11,0);
    feet_bl_z = this->x(14,0);
    feet_br_z = this->x(17,0);

    float meanz = this->feet_stance[0]*feet_fl_z +
                  this->feet_stance[1]*feet_fr_z +
                  this->feet_stance[2]*feet_bl_z +
                  this->feet_stance[3]*feet_br_z;

    return meanz;

}


void StateEstimation::getCoMPosition(Eigen::Vector3f* com_pos){
    Eigen::Vector3f com_pos_tmp;

    Eigen::Vector3f feet_fl, feet_fr, feet_bl, feet_br, base_pos;


    this->getFeetPosition("fl", &feet_fl);
    this->getFeetPosition("fr", &feet_fr);
    this->getFeetPosition("bl", &feet_bl);
    this->getFeetPosition("br", &feet_br);
    this->getBasePosition(&base_pos);


    feet_fl = this->base_rotation_matrix.transpose() * (feet_fl - base_pos);
    feet_fr = this->base_rotation_matrix.transpose() * (feet_fr - base_pos);
    feet_bl = this->base_rotation_matrix.transpose() * (feet_bl - base_pos);
    feet_br = this->base_rotation_matrix.transpose() * (feet_br - base_pos);


    com_pos_tmp = this->feet_stance[0]*feet_fl +
                  this->feet_stance[1]*feet_fr +
                  this->feet_stance[2]*feet_bl +
                  this->feet_stance[3]*feet_br;

    com_pos_tmp << com_pos_tmp.x(), com_pos_tmp.y(), base_pos.z();

    std::cout << com_pos_tmp.x() << " " << com_pos_tmp.y() << " " << com_pos_tmp.z() << std::endl;

    com_pos->operator=(com_pos_tmp);
}
