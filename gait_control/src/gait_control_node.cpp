#include "../include/gait_control_node.h"

void eigen2Point(geometry_msgs::Point *p, Eigen::Vector3f* vect){
    p->x = vect->operator()(0);
    p->y = vect->operator()(1);
    p->z = vect->operator()(2);

}



float clipValue(float val, float min, float max){
    if (val > max) val = max;
    if (val < min) val = min;
    return val;
}



GaitControl::GaitControl(ros::NodeHandle* n):
    robot_state(),
    joint_state(),
    feet_press(),
    vel_cmd(),
    body_pose_cmd(),
    joint_control_msg(),
    fl_trajectory("fl"),
    fr_trajectory("fr"),
    bl_trajectory("bl"),
    br_trajectory("br")
{
	nh = n;

	this->debug("INFO", "gait_control_node is running!");

	ros::Duration(7).sleep();

	this->robot_state.pose.position.x = 0;
    this->robot_state.pose.position.y = 0;
    this->robot_state.pose.position.z = 0;
    this->robot_state.pose.orientation.x = 0;
    this->robot_state.pose.orientation.y = 0;
    this->robot_state.pose.orientation.z = 0;
    this->robot_state.pose.orientation.w = 1;

    std::string urdf_path = ros::package::getPath("pavlov_mini_description") + std::string("/urdf/pavlov_mini.urdf");

	this->kinematics           = new Kinematics(urdf_path);
	this->polygonSupport       = new PredPolygonSupport();
	this->contactDetector      = new ContactDetection();
	this->torqController       = new torqueController(this->kinematics);
    this->torqStaticController = new torqueController(this->kinematics);
	this->stateEstimation      = new StateEstimation(this->kinematics);
	this->predModelController  = new PredictiveModeController(&this->fl_trajectory, &this->fr_trajectory, &this->bl_trajectory, &this->br_trajectory, this->kinematics);
    this->predModelControllerNormal = new PredictiveModeController(&this->fl_trajectory, &this->fr_trajectory, &this->bl_trajectory, &this->br_trajectory, this->kinematics);

    Eigen::Vector4f base_quaternion;
    base_quaternion << 0,0,0,1;
    float jointPos[12] = {0,0,0,0,0,0,0,0,0,0,0,0};

    this->imu_data.orientation.w = 1;
    this->imu_data.orientation.x = 0;
    this->imu_data.orientation.y = 0;
    this->imu_data.orientation.z = 0;
    this->imu_data.header.frame_id = "base_link";

    this->stateEstimation->setInitialState(base_quaternion, jointPos);

    this->kinematics->getLegDimentions();


    Eigen::Vector3f body_pos, body_rpy;
    body_pos << BODY_OFFSET_X, BODY_OFFSET_Y,BODY_INITIAL_POS_Z;
    body_rpy << BODY_INITIAL_ROLL, BODY_INITIAL_PITCH, BODY_INITIAL_YAW;
    this->kinematics->setBodyPose(body_pos, body_rpy);
    this->joint_control_msg.name = {"hip1_fl", "hip2_fl", "knee_fl",
                                    "hip1_fr", "hip2_fr", "knee_fr",
                                    "hip1_bl", "hip2_bl", "knee_bl",
                                    "hip1_br", "hip2_br", "knee_br"};
    this->joint_control_msg.position = {0,0,0,0,0,0,0,0,0,0,0,0};
    this->joint_control_msg.velocity = {0,0,0,0,0,0,0,0,0,0,0,0};
    this->joint_control_msg.effort   = {0,0,0,0,0,0,0,0,0,0,0,0};


    this->joint_state.name = {"hip1_fl", "hip2_fl", "knee_fl",
                              "hip1_fr", "hip2_fr", "knee_fr",
                              "hip1_bl", "hip2_bl", "knee_bl",
                              "hip1_br", "hip2_br", "knee_br"};
    this->joint_state.position = {0,0,0,0,0,0,0,0,0,0,0,0};
    this->joint_state.velocity = {0,0,0,0,0,0,0,0,0,0,0,0};
    this->joint_state.effort   = {0,0,0,0,0,0,0,0,0,0,0,0};


    this->feet_press.leg_name = {"fl", "fr", "bl", "br"};
    geometry_msgs::Point p;
    this->feet_press.pressure_vector = {p, p, p, p};
    this->feet_press.feet_stance = {1,1,1,1};

    this->feet_forces_msg.leg_name = {"fl", "fr", "bl", "br"};
    this->feet_forces_msg.pressure_vector = {p, p, p, p};
    this->feet_forces_msg.feet_stance = {1,1,1,1};

    // [publishers]
    this->jointControlPub     = nh->advertise<sensor_msgs::JointState>( "/pavlov_mini/joints_control", 10 );
    this->feetPressurePub     = nh->advertise<pavlov_mini_msgs::feet_pressure>( "pavlov_mini/feet_pressure",   10 );
    this->feetForceControlPub = nh->advertise<pavlov_mini_msgs::feet_pressure>( "pavlov_mini/feet_pressure_control",   10 );

	// [subscribers]
	this->robotStateSub   = nh->subscribe("/pavlov_mini/state_estimation", 1, &GaitControl::robotStateCB, this);
	this->imuDataSub      = nh->subscribe("/pavlov_mini/imu_data",         1, &GaitControl::imuDataCB,    this);

    this->torsoControlSub = nh->subscribe("/pavlov_mini/torso_pose_control", 1, &GaitControl::torsoControlCB, this);
    this->velCmdSub       = nh->subscribe("/pavlov_mini/vel_cmd",      1, &GaitControl::velCmdCB,     this);
    this->jointStateSub   = nh->subscribe("/pavlov_mini/joints_state", 1, &GaitControl::jointStateCB, this);

    // [services]
    this->setGaitService     = nh->advertiseService("/pavlov_mini/set_gait",     &GaitControl::setGaitCB,     this);
    this->setSequenceService = nh->advertiseService("/pavlov_mini/set_sequence", &GaitControl::setSequenceCB, this);


    // [clients]

    this->walk = false;
    this->set_initial_gait_file = false;
    this->setGaitFromFile("stop");
    this->set_initial_gait_file = true;

    this->imu_data.orientation.x = 0;
    this->imu_data.orientation.y = 0;
    this->imu_data.orientation.z = 0;
    this->imu_data.orientation.w = 1;


    this->initPhase();
    this->publishControlTimer = ros::Time::now();
    this->publishVisTimer     = ros::Time::now();

    this->dtTimer = ros::Time::now();

    this->available_imu_data = false;
    this->dtTimer_IMU = ros::Time::now();

    this->loop_counter = 0;
    this->robot_COM_z_pos = BODY_INITIAL_POS_Z;

}

GaitControl::~GaitControl(){
	delete nh;
	nh = NULL;
}



/************* callbacks *******************/

void GaitControl::robotStateCB(const pavlov_mini_msgs::robot_state::ConstPtr &msg){
    this->robot_state = *msg;
}

void GaitControl::imuDataCB(const sensor_msgs::Imu::ConstPtr &msg){
    this->imu_data = *msg;
    this->available_imu_data = true;



}

void GaitControl::jointStateCB(const sensor_msgs::JointState::ConstPtr &msg){
    this->joint_state = *msg;
}
void GaitControl::velCmdCB(const geometry_msgs::TwistStamped::ConstPtr &msg){
    this-> vel_cmd.twist.linear.x = clipValue( msg->twist.linear.x, -this->max_abs_vel_x, this->max_abs_vel_x  );
    this-> vel_cmd.twist.linear.y = clipValue( msg->twist.linear.y, -this->max_abs_vel_y, this->max_abs_vel_y  );
    this-> vel_cmd.twist.linear.z = clipValue( msg->twist.linear.z, -this->max_abs_vel_z, this->max_abs_vel_z  );

    this-> vel_cmd.twist.angular.x = clipValue( msg->twist.angular.x, -this->max_abs_vel_roll, this->max_abs_vel_roll  );
    this-> vel_cmd.twist.angular.y = clipValue( msg->twist.angular.y, -this->max_abs_vel_pitch, this->max_abs_vel_pitch  );
    this-> vel_cmd.twist.angular.z = clipValue( msg->twist.angular.z, -this->max_abs_vel_yaw, this->max_abs_vel_yaw  );

}
void GaitControl::torsoControlCB(const geometry_msgs::PoseStamped::ConstPtr &msg){
    this->body_pose_cmd = *msg;
    tf::Quaternion q(
            msg->pose.orientation.x,
            msg->pose.orientation.y,
            msg->pose.orientation.z,
            msg->pose.orientation.w);

    tf::Matrix3x3 m(q);
    double roll, pitch, yaw;
    m.getRPY(roll, pitch, yaw);


    Eigen::Vector3f body_pos, body_rpy;
    body_rpy << roll, pitch, yaw;
    body_pos << msg->pose.position.x, msg->pose.position.y, msg->pose.position.z;

    //this->kinematics->setBodyPose(body_pos, body_rpy);
}


bool GaitControl::setGaitCB(pavlov_mini_msgs::set_gait::Request &req, pavlov_mini_msgs::set_gait::Response &res){
    this->setGaitFromFile(req.gait_filename);

    res.result = true;
    res.info   = "set gait succesfully";
    return true;
}


bool GaitControl::setSequenceCB(pavlov_mini_msgs::set_gait::Request &req, pavlov_mini_msgs::set_gait::Response &res){
    this->setSequenceFromFile(req.gait_filename);

    res.result = true;
    res.info   = "set sequence succesfully";
    return true;
}



/*
void GaitControl::subCallback(const std_msgs::String::ConstPtr &msg){
	this->debug("SUBCALLBACK", msg->data);
}

*/

void GaitControl::initPhase(){
    this->phase = 0;
    this->phaseTimer = ros::Time::now();
    this->phase_counter = 0;
}

void GaitControl::getPhase(double period){
    double dt =  (ros::Time::now() - this->phaseTimer).toSec();
    this->phase_counter = (int) dt/period;
    double t = dt / period - this->phase_counter;
    this->phase = std::fmod(dt, period)/period;
    //std::cout << this->phase << std::endl;
}


void GaitControl::getLegStateAngles (std::string leg, Eigen::Vector3f* angles){
    if (leg == "fl"){
        angles->operator()(0) = this->joint_state.position[0];
        angles->operator()(1) = this->joint_state.position[1];
        angles->operator()(2) = this->joint_state.position[2];
    }
    if (leg == "fr"){
        angles->operator()(0) = this->joint_state.position[3];
        angles->operator()(1) = this->joint_state.position[4];
        angles->operator()(2) = this->joint_state.position[5];
    }
    if (leg == "bl"){
        angles->operator()(0) = this->joint_state.position[6];
        angles->operator()(1) = this->joint_state.position[7];
        angles->operator()(2) = this->joint_state.position[8];
    }
    if (leg == "br"){
        angles->operator()(0) = this->joint_state.position[9];
        angles->operator()(1) = this->joint_state.position[10];
        angles->operator()(2) = this->joint_state.position[11];
    }
}
void GaitControl::getLegStateTorques(std::string leg, Eigen::Vector3f* torques){
    if (leg == "fl"){
        torques->operator()(0) = this->joint_state.effort[0];
        torques->operator()(1) = this->joint_state.effort[1];
        torques->operator()(2) = -this->joint_state.effort[2];
    }
    if (leg == "fr"){
        torques->operator()(0) = this->joint_state.effort[3];
        torques->operator()(1) = this->joint_state.effort[4];
        torques->operator()(2) = -this->joint_state.effort[5];
    }
    if (leg == "bl"){
        torques->operator()(0) = this->joint_state.effort[6];
        torques->operator()(1) = this->joint_state.effort[7];
        torques->operator()(2) = -this->joint_state.effort[8];
    }
    if (leg == "br"){
        torques->operator()(0) = this->joint_state.effort[9];
        torques->operator()(1) = this->joint_state.effort[10];
        torques->operator()(2) = -this->joint_state.effort[11];
    }
}





/************ main loop ****************/
void GaitControl::mainNodeThread(){
    std::cout << std::setprecision(2) << std::fixed;
    float dt = (ros::Time::now() - this->dtTimer).toSec();
    this->dtTimer = ros::Time::now();

    /*
    //double period = 1.2;
    double period = 2;

    double prop_swing_time = 0.4;
    float height = 0.09;
    //float height = 0.06;
    float sigma1  = 0.12;
    float sigma2  = 0.12;
    float sigma1_ = 0.12;
    float sigma2_ = 0.12;
    float offset  = 0.5;
    */

    /*********************************************************************************
     ************************** GET JOINT FEEDBACK ***********************************
     *********************************************************************************/

    /************ GET CURRENT ANGLES **************/
    Eigen::Vector3f curr_angles_fl, curr_angles_fr, curr_angles_bl, curr_angles_br;
    this->getLegStateAngles("fl", &curr_angles_fl);
    this->getLegStateAngles("fr", &curr_angles_fr);
    this->getLegStateAngles("bl", &curr_angles_bl);
    this->getLegStateAngles("br", &curr_angles_br);

    /************ GET CURRENT FEET POS (FROM REF) **************/
    Eigen::Vector3f curr_feet_fl_pos, curr_feet_fr_pos, curr_feet_bl_pos, curr_feet_br_pos;
    this->kinematics->feetPosFromRef("fl", curr_angles_fl, &curr_feet_fl_pos);
    this->kinematics->feetPosFromRef("fr", curr_angles_fr, &curr_feet_fr_pos);
    this->kinematics->feetPosFromRef("bl", curr_angles_bl, &curr_feet_bl_pos);
    this->kinematics->feetPosFromRef("br", curr_angles_br, &curr_feet_br_pos);

    /************ GET CURRENT FEET PO (FROM BASE) **************/
    Eigen::Vector3f curr_feet_fl_pos_fromBase, curr_feet_fr_pos_fromBase, curr_feet_bl_pos_fromBase, curr_feet_br_pos_fromBase;
    this->kinematics->feetPosFromBase("fl", curr_angles_fl, &curr_feet_fl_pos_fromBase);
    this->kinematics->feetPosFromBase("fr", curr_angles_fr, &curr_feet_fr_pos_fromBase);
    this->kinematics->feetPosFromBase("bl", curr_angles_bl, &curr_feet_bl_pos_fromBase);
    this->kinematics->feetPosFromBase("br", curr_angles_br, &curr_feet_br_pos_fromBase);

    /************ GET CURRENT TORQUES **************/
    Eigen::Vector3f curr_torques_fl, curr_torques_fr, curr_torques_bl, curr_torques_br;
    this->getLegStateTorques("fl", &curr_torques_fl);
    this->getLegStateTorques("fr", &curr_torques_fr);
    this->getLegStateTorques("bl", &curr_torques_bl);
    this->getLegStateTorques("br", &curr_torques_br);

    /************ GET CURRENT FEET FORCE **************/
    Eigen::Vector3f curr_force_fl, curr_force_fr, curr_force_bl, curr_force_br;
    this->kinematics->getForceReaction("fl", curr_angles_fl, curr_torques_fl, &curr_force_fl);
    this->kinematics->getForceReaction("fr", curr_angles_fr, curr_torques_fr, &curr_force_fr);
    this->kinematics->getForceReaction("bl", curr_angles_bl, curr_torques_bl, &curr_force_bl);
    this->kinematics->getForceReaction("br", curr_angles_br, curr_torques_br, &curr_force_br);

    //std::cout << force_br.x() << " " << force_br.y() << " " << force_br.z()  << std::endl;

    /************ GET CURRENT CONSTANT STANCE **************/
    bool curr_stance_fl, curr_stance_fr, curr_stance_bl, curr_stance_br;
    curr_stance_fl = this->fl_trajectory.getStance();//this->contactDetector->isFeetStanding(&curr_force_fl);
    curr_stance_fr = this->fr_trajectory.getStance();//this->contactDetector->isFeetStanding(&curr_force_fr);
    curr_stance_bl = this->bl_trajectory.getStance();//this->contactDetector->isFeetStanding(&curr_force_bl);
    curr_stance_br = this->br_trajectory.getStance();//this->contactDetector->isFeetStanding(&curr_force_br);

    //std::cout << curr_stance_fl << " " << curr_stance_fr << " " << curr_stance_bl  << " " << curr_stance_br << std::endl;



    /************ CENTER OF PRESSURE **************/

    Eigen::Vector3f base_cop;

    int curr_stance_feets = curr_stance_fl + curr_stance_fr + curr_stance_bl + curr_stance_br;

    base_cop = (curr_stance_fl*curr_feet_fl_pos_fromBase +
               curr_stance_fr*curr_feet_fr_pos_fromBase +
               curr_stance_bl*curr_feet_bl_pos_fromBase +
               curr_stance_br*curr_feet_br_pos_fromBase)/curr_stance_feets;

    base_cop = (curr_feet_fl_pos_fromBase +
                curr_feet_fr_pos_fromBase +
                curr_feet_bl_pos_fromBase +
                curr_feet_br_pos_fromBase)/4;


    base_cop.z() = -base_cop.z();


    //std::cout << curr_feet_fl_pos_fromBase.x() << " " << curr_feet_fl_pos_fromBase.y() << " " << curr_feet_fl_pos_fromBase.z() << std::endl;
    //std::cout << curr_feet_fr_pos_fromBase.x() << " " << curr_feet_fr_pos_fromBase.y() << " " << curr_feet_fr_pos_fromBase.z() << std::endl  << std::endl;
    //std::cout << base_cop.x() << " " << base_cop.y() << " " << base_cop.z() << std::endl;

    /**************************************************************************
    ***************************** STATE ESTIMATION ****************************
    ****************************************************************************/


    //if (this->available_imu_data) {


        float dt_imu = (ros::Time::now() - this->dtTimer_IMU).toSec();
        this->dtTimer_IMU = ros::Time::now();


        Eigen::Vector4f base_quaternion;
        base_quaternion << this->imu_data.orientation.x,
                           this->imu_data.orientation.y,
                           this->imu_data.orientation.z,
                           this->imu_data.orientation.w;





        Eigen::Vector3f acc;
        acc << this->imu_data.linear_acceleration.x,
               this->imu_data.linear_acceleration.y,
               this->imu_data.linear_acceleration.z;




        bool feet_stance[4] = {curr_stance_fl, curr_stance_fr, curr_stance_bl, curr_stance_br};



        this->stateEstimation->setInput(base_quaternion, acc, feet_stance, dt_imu);
        this->stateEstimation->predictState();


        //Eigen::Vector3f estimated_base_pos_;
        //this->stateEstimation->getBasePosition(&estimated_base_pos_);
        //std::cout << "predcit state:   " << estimated_base_pos_.x() << " " << estimated_base_pos_.y() << " " << estimated_base_pos_.z() << std::endl;

        float joint_pos[12] = {curr_angles_fl.x(), curr_angles_fl.y(), curr_angles_fl.z(),
                               curr_angles_fr.x(), curr_angles_fr.y(), curr_angles_fr.z(),
                               curr_angles_bl.x(), curr_angles_bl.y(), curr_angles_bl.z(),
                               curr_angles_br.x(), curr_angles_br.y(), curr_angles_br.z()};

        float joint_vel[12] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        float feet_height[4] = {0, 0, 0, 0};

        Eigen::Vector3f ang_vel;
        ang_vel << this->imu_data.angular_velocity.x,
                   this->imu_data.angular_velocity.y,
                   this->imu_data.angular_velocity.z;

        this->stateEstimation->setMeasurement(ang_vel, joint_pos, joint_vel, feet_height);
        this->stateEstimation->updateState();


        this->available_imu_data = false;


    //}

    Eigen::Vector3f estimated_base_pos;
    this->stateEstimation->getBasePosition(&estimated_base_pos);


    Eigen::Vector3f estimated_base_vel;
    this->stateEstimation->getBaseVelocity(&estimated_base_vel);


    Eigen::Vector3f estimated_base_ang_vel;
    this->stateEstimation->getBaseAngularVelocity(&estimated_base_ang_vel);


    Eigen::Vector3f base_acc_from_base;
    this->stateEstimation->getBaseAccelerationFromBaseRef(&base_acc_from_base);

    Eigen::Matrix3f base_rotation_matrix;
    this->stateEstimation->getBaseRotationMatrix(&base_rotation_matrix);

    Eigen::Vector3f estimated_feet_fl, estimated_feet_fr, estimated_feet_bl, estimated_feet_br;
    this->stateEstimation->getFeetPosition("fl", &estimated_feet_fl);
    this->stateEstimation->getFeetPosition("fr", &estimated_feet_fr);
    this->stateEstimation->getFeetPosition("bl", &estimated_feet_bl);
    this->stateEstimation->getFeetPosition("br", &estimated_feet_br);


    /*********************************************************************************
     * **************************** PREDICTIVE SUPPORT POLYGON **************************
     *********************************************************************************/

    /**************** OBTAIN WEIGHT FACTOR FOR SYPPORT POLYGON  ***************/
    float weight_factor_fl = this->fl_trajectory.getWeightingFactor(this->wfactor_stance_sigma1, this->wfactor_stance_sigma2, this->wfactor_swing_sigma1, this->wfactor_swing_sigma2, this->wfactor_offset);
    float weight_factor_fr = this->fr_trajectory.getWeightingFactor(this->wfactor_stance_sigma1, this->wfactor_stance_sigma2, this->wfactor_swing_sigma1, this->wfactor_swing_sigma2, this->wfactor_offset);
    float weight_factor_bl = this->bl_trajectory.getWeightingFactor(this->wfactor_stance_sigma1, this->wfactor_stance_sigma2, this->wfactor_swing_sigma1, this->wfactor_swing_sigma2, this->wfactor_offset);
    float weight_factor_br = this->br_trajectory.getWeightingFactor(this->wfactor_stance_sigma1, this->wfactor_stance_sigma2, this->wfactor_swing_sigma1, this->wfactor_swing_sigma2, this->wfactor_offset);

    //std::cout << weight_factor_fl << " " << weight_factor_fr << " " << weight_factor_bl << " " << weight_factor_br << std::endl;


    bool stance_fl, stance_fr, stance_bl, stance_br;
    stance_fl = this->fl_trajectory.getStance();
    stance_fr = this->fr_trajectory.getStance();
    stance_bl = this->bl_trajectory.getStance();
    stance_br = this->br_trajectory.getStance();
    //std::cout << stance_fl << " " << stance_fr << " " << stance_bl << " " << stance_br << std::endl;

    /**************** SET BASE POSE ***************/
    //std::cout << curr_feet_fl_pos_fromBase.x() << " " << curr_feet_fl_pos_fromBase.y() << " " << curr_feet_fl_pos_fromBase.z() << std::endl;
    //std::cout << curr_feet_fr_pos_fromBase.x() << " " << curr_feet_fr_pos_fromBase.y() << " " << curr_feet_fr_pos_fromBase.z() << std::endl;
    //std::cout << curr_feet_bl_pos_fromBase.x() << " " << curr_feet_bl_pos_fromBase.y() << " " << curr_feet_bl_pos_fromBase.z() << std::endl;
    //std::cout << curr_feet_br_pos_fromBase.x() << " " << curr_feet_br_pos_fromBase.y() << " " << curr_feet_br_pos_fromBase.z() << std::endl << std::endl;


    Eigen::Vector3f base_position_offset;
    base_position_offset << this->base_offset_x,// + psp_body_pos.x()*this->factor_predictive_support_polygon,
                            this->base_offset_y,// + psp_body_pos.y()*this->factor_predictive_support_polygon,
                            0;// + psp_body_pos.y()*this->factor_predictive_support_polygon, 0;

    this->polygonSupport->setLeg("fl", curr_feet_fl_pos_fromBase + base_position_offset, weight_factor_fl);
    this->polygonSupport->setLeg("fr", curr_feet_fr_pos_fromBase + base_position_offset, weight_factor_fr);
    this->polygonSupport->setLeg("bl", curr_feet_bl_pos_fromBase + base_position_offset, weight_factor_bl);
    this->polygonSupport->setLeg("br", curr_feet_br_pos_fromBase + base_position_offset, weight_factor_br);

    Eigen::Vector3f psp_body_pos;
    this->polygonSupport->getDesiredBodyPos(&psp_body_pos);





    psp_body_pos = base_rotation_matrix * this->factor_predictive_support_polygon * psp_body_pos + estimated_base_pos;

    //std::cout << "PSP: " << psp_body_pos.x() << " " << psp_body_pos.y() << " " << psp_body_pos.z() << std::endl;
    //std::cout << "EST: " << estimated_base_pos.x() << " " << estimated_base_pos.y() << " " << estimated_base_pos.z() << std::endl;







    /************************************************************************/
    /********************* FORCE CONTROLLER *******************************/
    /***********************************************************************/
    Eigen::Vector3f terrain_norm, terrain_t1, terrain_t2;
    terrain_norm << 0, 0, 1;
    terrain_t1 << 1, 0, 0;
    terrain_t2 << 0, 1, 0;
    float mu = 0.25;

    Eigen::Vector3f base_vel_weight;

    Eigen::Vector3f cur_base_pos, des_base_vel, des_base_ang_vel, des_base_pos;

    float estimated_z_pos = this->stateEstimation->getMeanFeetHeight();

    //this->stateEstimation->getCoMPosition(&cur_base_pos);



    cur_base_pos
            << estimated_base_pos.x(), // base_cop.x(),// + psp_body_pos.x()*this->factor_predictive_support_polygon,
            estimated_base_pos.y(), //base_cop.y(),// + psp_body_pos.y()*this->factor_predictive_support_polygon,
            estimated_base_pos.z(); //BODY_INITIAL_POS_Z;// - estimated_z_pos;

    cur_base_pos = estimated_base_pos;// + base_rotation_matrix * base_position_offset;

    this->robot_COM_z_pos = this->robot_COM_z_pos + this->vel_cmd.twist.linear.z * dt;

    //std::cout << this->robot_COM_z_pos << std::endl;

    des_base_pos << psp_body_pos.x(),
            psp_body_pos.y(),
            this->robot_COM_z_pos;


    des_base_vel << this->vel_cmd.twist.linear.x, this->vel_cmd.twist.linear.y, this->vel_cmd.twist.linear.z;
    des_base_vel = base_rotation_matrix * des_base_vel;

    des_base_ang_vel << this->vel_cmd.twist.angular.x, this->vel_cmd.twist.angular.y, this->vel_cmd.twist.angular.z;
    des_base_ang_vel = base_rotation_matrix * des_base_ang_vel;

    //std::cout << des_base_ang_vel.x() << " " << des_base_ang_vel.y() << " " << des_base_ang_vel.z() << std::endl;
    //std::cout << cur_base_pos.x() << " " << cur_base_pos.y() << " " << cur_base_pos.z() << std::endl << std::endl;


    Eigen::Matrix3f des_base_rot;
    des_base_rot << 1, 0, 0,
                    0, 1, 0,
                    0, 0, 1;


    Eigen::Vector3f des_feet_dir_fl, des_feet_dir_fr, des_feet_dir_bl, des_feet_dir_br;
    des_feet_dir_fl.setZero();
    des_feet_dir_fr.setZero();
    des_feet_dir_bl.setZero();
    des_feet_dir_br.setZero();

    if (this->enable_force_controller) {

        this->torqController->setFeetForceLimits(MIN_FEET_FORCE, MAX_FEET_FORCE);
        this->torqStaticController->setFeetForceLimits(MIN_FEET_FORCE, MAX_FEET_FORCE);

        this->torqController->setTerrainPlane("fl", mu, terrain_norm, terrain_t1, terrain_t2);
        this->torqController->setTerrainPlane("fr", mu, terrain_norm, terrain_t1, terrain_t2);
        this->torqController->setTerrainPlane("bl", mu, terrain_norm, terrain_t1, terrain_t2);
        this->torqController->setTerrainPlane("br", mu, terrain_norm, terrain_t1, terrain_t2);

        this->torqStaticController->setTerrainPlane("fl", mu, terrain_norm, terrain_t1, terrain_t2);
        this->torqStaticController->setTerrainPlane("fr", mu, terrain_norm, terrain_t1, terrain_t2);
        this->torqStaticController->setTerrainPlane("bl", mu, terrain_norm, terrain_t1, terrain_t2);
        this->torqStaticController->setTerrainPlane("br", mu, terrain_norm, terrain_t1, terrain_t2);

        Eigen::Vector3f curr_feet_from_base_fl, curr_feet_from_base_fr, curr_feet_from_base_bl, curr_feet_from_base_br;

        this->kinematics->feetPosFromBase("fl", curr_angles_fl, &curr_feet_from_base_fl);
        this->kinematics->feetPosFromBase("fr", curr_angles_fr, &curr_feet_from_base_fr);
        this->kinematics->feetPosFromBase("bl", curr_angles_bl, &curr_feet_from_base_bl);
        this->kinematics->feetPosFromBase("br", curr_angles_br, &curr_feet_from_base_br);

        this->torqController->setLegState("fl", base_rotation_matrix * curr_feet_from_base_fl, curr_stance_fl);
        this->torqController->setLegState("fr", base_rotation_matrix * curr_feet_from_base_fr, curr_stance_fr);
        this->torqController->setLegState("bl", base_rotation_matrix * curr_feet_from_base_bl, curr_stance_bl);
        this->torqController->setLegState("br", base_rotation_matrix * curr_feet_from_base_br, curr_stance_br);

        this->torqStaticController->setLegState("fl", base_rotation_matrix * curr_feet_from_base_fl, curr_stance_fl);
        this->torqStaticController->setLegState("fr", base_rotation_matrix * curr_feet_from_base_fr, curr_stance_fr);
        this->torqStaticController->setLegState("bl", base_rotation_matrix * curr_feet_from_base_bl, curr_stance_bl);
        this->torqStaticController->setLegState("br", base_rotation_matrix * curr_feet_from_base_br, curr_stance_br);


        //base_vel_from_base << 0,0,0;
        //base_rotation_matrix = des_base_rot;
        //base_ang_vel_from_base << 0,0,0;

        this->torqController->setRobotState(cur_base_pos, estimated_base_vel, base_rotation_matrix,
                                            estimated_base_ang_vel);
        this->torqController->setRobotControl(des_base_pos, des_base_vel, des_base_rot, des_base_ang_vel);

        this->torqStaticController->setRobotState(cur_base_pos, estimated_base_vel, base_rotation_matrix,
                                                  estimated_base_ang_vel);


        this->torqStaticController->setRobotControl(cur_base_pos, estimated_base_vel, base_rotation_matrix,
                                                    estimated_base_ang_vel);

        this->torqController->computeForces();
        this->torqStaticController->computeForces();

        Eigen::Vector3f des_force_fl, des_force_fr, des_force_bl, des_force_br;
        this->torqController->getForces(&des_force_fl, &des_force_fr, &des_force_bl, &des_force_br);


        //*********** APPROXIMATION FOR NON-TORQUE ACTUATORS

        Eigen::Vector3f normal_force_fl, normal_force_fr, normal_force_bl, normal_force_br;
        this->torqStaticController->getForces(&normal_force_fl, &normal_force_fr, &normal_force_bl, &normal_force_br);
        //std::cout << normal_force_fl.x() << " " << normal_force_fl.y() << " " << normal_force_fl.z() << std::endl;
        //std::cout << normal_force_fr.x() << " " << normal_force_fr.y() << " " << normal_force_fr.z() << std::endl;
        //std::cout << normal_force_bl.x() << " " << normal_force_bl.y() << " " << normal_force_bl.z() << std::endl;
        //std::cout << normal_force_br.x() << " " << normal_force_br.y() << " " << normal_force_br.z() << std::endl;


        std::cout << "--------------------------" << std::endl;

        //std::cout << des_force_fl.x() << " " << des_force_fl.y() << " " << des_force_fl.z() << std::endl;
        //std::cout << des_force_fr.x() << " " << des_force_fr.y() << " " << des_force_fr.z() << std::endl;
        //std::cout << des_force_bl.x() << " " << des_force_bl.y() << " " << des_force_bl.z() << std::endl;
        //std::cout << des_force_br.x() << " " << des_force_br.y() << " " << des_force_br.z() << std::endl;

        //Eigen::Vector3f force_weight_normal;
        //int leg_stance_count = 0;
        //bool stance_legs[4] = {curr_stance_fl, curr_stance_fr, curr_stance_bl, curr_stance_br};

        //for (int i=0; i< 4; i++){
        //    if (stance_legs[i]) leg_stance_count+= 1;
        //}
        //force_weight_normal << 0,0, (9.8*ROBOT_TOTAL_MASS)/leg_stance_count;

        des_feet_dir_fl = normal_force_fl - des_force_fl;
        des_feet_dir_fr = normal_force_fr - des_force_fr;
        des_feet_dir_bl = normal_force_bl - des_force_bl;
        des_feet_dir_br = normal_force_br - des_force_br;

        base_vel_weight << this->torqController->getKvel(), this->torqController->getKvel(), this->torqController->getKvel();

        //***************************************************

    }

    /**************************************************************************/
    /*************************** PREDICTIVE MODEL CONTROLLER *****************/
    /**************************************************************************/
    if (this->enable_pmc_controller){
        std::cout << "hola!" << std::endl;
        this->predModelController->setTerrainPlane("fl", mu, terrain_norm, terrain_t1, terrain_t2);
        this->predModelController->setTerrainPlane("fr", mu, terrain_norm, terrain_t1, terrain_t2);
        this->predModelController->setTerrainPlane("bl", mu, terrain_norm, terrain_t1, terrain_t2);
        this->predModelController->setTerrainPlane("br", mu, terrain_norm, terrain_t1, terrain_t2);



        tf::Matrix3x3 base_rotation_matrix_tf;

        tf::matrixEigenToTF(base_rotation_matrix.cast <double> (), base_rotation_matrix_tf);
        double yaw, pitch, roll;
        base_rotation_matrix_tf.getEulerYPR(yaw, pitch, roll);

        Eigen::Vector3f estimated_base_angles;
        estimated_base_angles << roll, pitch, yaw;


        this->predModelController->setRobotState(cur_base_pos, estimated_base_vel, estimated_base_angles, estimated_base_ang_vel);

        this->predModelController->setRobotControl(des_base_vel, des_base_ang_vel);


        this->predModelController->computeForces( this->phase );
        //this->predModelController->computeForcesQPoases( this->phase );

        Eigen::Vector3f des_force_fl_pmc, des_force_fr_pmc, des_force_bl_pmc, des_force_br_pmc;
        this->predModelController->getForces(&des_force_fl_pmc, &des_force_fr_pmc, &des_force_bl_pmc, &des_force_br_pmc);


        Eigen::Vector3f des_feet_dir_fl_pmc, des_feet_dir_fr_pmc, des_feet_dir_bl_pmc, des_feet_dir_br_pmc;
        des_feet_dir_fl =   des_force_fl_pmc ;
        des_feet_dir_fr =   des_force_fr_pmc;
        des_feet_dir_bl =   des_force_bl_pmc ;
        des_feet_dir_br =   des_force_br_pmc ;

        this->predModelController->getBaseVelWeight(&base_vel_weight);

    }


    geometry_msgs::Point p_des_force_fl, p_des_force_fr, p_des_force_bl, p_des_force_br;
    //eigen2Point(&p_des_force_fl, &des_force_fl);
    //eigen2Point(&p_des_force_fr, &des_force_fr);
    //eigen2Point(&p_des_force_bl, &des_force_bl);
    //eigen2Point(&p_des_force_br, &des_force_br);

    eigen2Point(&p_des_force_fl, &des_feet_dir_fl);
    eigen2Point(&p_des_force_fr, &des_feet_dir_fr);
    eigen2Point(&p_des_force_bl, &des_feet_dir_bl);
    eigen2Point(&p_des_force_br, &des_feet_dir_br);


    this->feet_forces_msg.pressure_vector[0] = p_des_force_fl;
    this->feet_forces_msg.pressure_vector[1] = p_des_force_fr;
    this->feet_forces_msg.pressure_vector[2] = p_des_force_bl;
    this->feet_forces_msg.pressure_vector[3] = p_des_force_br;

    this->feet_forces_msg.feet_stance = {curr_stance_fl, curr_stance_fr, curr_stance_bl, curr_stance_br};


    /**************************************************************************/
    /************************** MAKE STEP *************************************/
    /**************************************************************************/



    this->getPhase(this->gait_period);
    Eigen::Vector3f feet_fl_pos, feet_fr_pos, feet_bl_pos, feet_br_pos, desired_base_vel, desired_body_pos, body_pos, body_rpy;

    /**************** GET BODY VELOCITY COMMAND ************************/
    desired_base_vel << 0,0,0;
    if (this->walk) {
    //    desired_base_vel << 0.0 + this->vel_cmd.twist.linear.x, this->vel_cmd.twist.linear.y, 0;
    }
    else{
        if (estimated_base_vel.norm() >= MIN_BASE_VELOCITY_FOR_REACTION){
            this->initPhase();
            this->fl_trajectory.setWalking(true);
            this->fr_trajectory.setWalking(true);
            this->bl_trajectory.setWalking(true);
            this->br_trajectory.setWalking(true);
        }
        else{
            this->fl_trajectory.setWalking(false);
            this->fr_trajectory.setWalking(false);
            this->bl_trajectory.setWalking(false);
            this->br_trajectory.setWalking(false);
        }
    }

    //std::cout << this->phase << std::endl;
    /**************** GET FEET POSITON COMMAND (FROM REF) ***************/

    //std::cout << this->robot_state.linear_velocity.x << " " << this->robot_state.linear_velocity.y << " " << this->robot_state.linear_velocity.z << std::endl;



    Eigen::Vector3f des_base_vel_fromBase_, des_base_vel_fromBase, est_base_vel_fromBase;

    des_base_vel_fromBase_ =  base_rotation_matrix.transpose() * des_base_vel ;

    des_base_vel_fromBase <<  des_base_vel_fromBase_.x() * base_vel_weight.x(),
                              des_base_vel_fromBase_.y() * base_vel_weight.y(),
                              des_base_vel_fromBase_.z() * base_vel_weight.z();


    est_base_vel_fromBase =  0.2*base_rotation_matrix.transpose() * estimated_base_vel ;

    this->fl_trajectory.makeStep(base_rotation_matrix.transpose() * des_feet_dir_fl,  des_base_vel_fromBase, est_base_vel_fromBase, this->phase, cur_base_pos.z(), &feet_fl_pos);
    this->fr_trajectory.makeStep(base_rotation_matrix.transpose() * des_feet_dir_fr,  des_base_vel_fromBase, est_base_vel_fromBase, this->phase, cur_base_pos.z(), &feet_fr_pos);
    this->bl_trajectory.makeStep(base_rotation_matrix.transpose() * des_feet_dir_bl,  des_base_vel_fromBase, est_base_vel_fromBase, this->phase, cur_base_pos.z(), &feet_bl_pos);
    this->br_trajectory.makeStep(base_rotation_matrix.transpose() * des_feet_dir_br,  des_base_vel_fromBase, est_base_vel_fromBase, this->phase, cur_base_pos.z(), &feet_br_pos);


    /******************* OBTAIN JOINT ANGLE COMMANDS ************/


    feet_fl_pos << feet_fl_pos.x(), feet_fl_pos.y(), feet_fl_pos.z() - this->robot_COM_z_pos;
    feet_fr_pos << feet_fr_pos.x(), feet_fr_pos.y(), feet_fr_pos.z() - this->robot_COM_z_pos;
    feet_bl_pos << feet_bl_pos.x(), feet_bl_pos.y(), feet_bl_pos.z() - this->robot_COM_z_pos;
    feet_br_pos << feet_br_pos.x(), feet_br_pos.y(), feet_br_pos.z() - this->robot_COM_z_pos;


    Eigen::Vector3f angles_fl, angles_fr, angles_bl, angles_br;
    bool hr_fl = this->kinematics->IK_leg_fromRef("fl", feet_fl_pos, &angles_fl);
    bool hr_fr = this->kinematics->IK_leg_fromRef("fr", feet_fr_pos, &angles_fr);
    bool hr_bl = this->kinematics->IK_leg_fromRef("bl", feet_bl_pos, &angles_bl);
    bool hr_br = this->kinematics->IK_leg_fromRef("br", feet_br_pos, &angles_br);



    /******************* SEND JOINT COMMANDS ************/
    if((hr_fl)&&(hr_fr)&&(hr_bl)&&(hr_br)) {    // if IK Reachability is possible



        this->joint_control_msg.position = {angles_fl.x(), angles_fl.y(), angles_fl.z(),
                                            angles_fr.x(), angles_fr.y(), angles_fr.z(),
                                            angles_bl.x(), angles_bl.y(), angles_bl.z(),
                                            angles_br.x(), angles_br.y(), angles_br.z()};


    }

    this->publishData();




    //this->loop_counter += 1;


}



void GaitControl::publishData(){
    double dt = (ros::Time::now() - this->publishControlTimer).toSec();
    if (dt > 1.0/PUBLISH_FREQ_CONTROL){

        this->joint_control_msg.header.stamp = ros::Time::now();
        this->jointControlPub.publish(this->joint_control_msg);
        this->publishControlTimer = ros::Time::now();
    }

    double dt2 = (ros::Time::now() - this->publishVisTimer).toSec();
    if (dt2 > 1.0/PUBLISH_FREQ_VIS){

        this->feet_press.header.stamp = ros::Time::now();
        this->feetPressurePub.publish(this->feet_press);

        this->feet_forces_msg.header.stamp = ros::Time::now();
        this->feetForceControlPub.publish(this->feet_forces_msg);


        Eigen::Vector3f estimated_base_pos;
        this->stateEstimation->getBasePosition(&estimated_base_pos);

        std::cout << estimated_base_pos.x() << " " << estimated_base_pos.y() << " " << estimated_base_pos.z() << std::endl;
        //std::cout << "_________________" << std::endl;


        tf::Vector3 robot_pos(estimated_base_pos.x(), estimated_base_pos.y(), estimated_base_pos.z());
        tf::Quaternion robot_q( this->imu_data.orientation.x, this->imu_data.orientation.y, this->imu_data.orientation.z, this->imu_data.orientation.w );

        tf::Transform robot_tf;
        robot_tf.setOrigin( robot_pos );
        robot_tf.setRotation( robot_q );


        //this->tf_br.sendTransform(tf::StampedTransform(robot_tf, this->imu_data.header.stamp, "world", "base_link"));
        this->tf_br.sendTransform(tf::StampedTransform(robot_tf, ros::Time::now(), "world", "base_link"));
        this->publishVisTimer = ros::Time::now();
    }


}




void GaitControl::setGaitFromFile(std::string file){

    std::string file_dir = ros::package::getPath("gait_control") + std::string("/gaits/") + file + std::string(".gait");

    std::ifstream infile(file_dir);
    if(!infile.good()){
        ROS_ERROR("Gait file does not exist, remember not to include extension '.gait' in the name");
        return;
    }

    ConfigFile cf(file_dir);

    bool walk_ = cf.Value("gait settings", "walk");

    //this->fl_trajectory.setWalking(this->walk);
    //this->fr_trajectory.setWalking(this->walk);
    //this->bl_trajectory.setWalking(this->walk);
    //this->br_trajectory.setWalking(this->walk);


    if ((walk_)||(!this->set_initial_gait_file)) {


        this->gait_period = cf.Value("gait settings", "gait_period");
        float prop_swing_time = cf.Value("gait settings", "prop_swing_time");
        float fl_phase_offset = cf.Value("gait settings", "fl_phase_offset");
        float fr_phase_offset = cf.Value("gait settings", "fr_phase_offset");
        float bl_phase_offset = cf.Value("gait settings", "bl_phase_offset");
        float br_phase_offset = cf.Value("gait settings", "br_phase_offset");

        float feet_offset_x = cf.Value("Feet and base settings", "feet_offset_x");
        float feet_offset_y = cf.Value("Feet and base settings", "feet_offset_y");
        this->base_offset_x = cf.Value("Feet and base settings", "base_offset_x");
        this->base_offset_y = cf.Value("Feet and base settings", "base_offset_y");
        float step_height = cf.Value("Feet and base settings", "step_height");
        this->force_contact_threshold = cf.Value("Feet and base settings", "force_contact_threshold");
        this->capture_point_velocity_factor = cf.Value("Feet and base settings", "capture_point_velocity_factor");

        this->fl_trajectory.setTrajectoryParams(this->gait_period, prop_swing_time, fl_phase_offset, step_height,
                                                feet_offset_x, feet_offset_y, this->base_offset_x, this->base_offset_y);
        this->fr_trajectory.setTrajectoryParams(this->gait_period, prop_swing_time, fr_phase_offset, step_height,
                                                feet_offset_x, feet_offset_y, this->base_offset_x, this->base_offset_y);
        this->bl_trajectory.setTrajectoryParams(this->gait_period, prop_swing_time, bl_phase_offset, step_height,
                                                feet_offset_x, feet_offset_y, this->base_offset_x, this->base_offset_y);
        this->br_trajectory.setTrajectoryParams(this->gait_period, prop_swing_time, br_phase_offset, step_height,
                                                feet_offset_x, feet_offset_y, this->base_offset_x, this->base_offset_y);


        this->factor_predictive_support_polygon = cf.Value("Predictive support polygon settings",
                                                           "factor_predictive_support_polygon");
        this->wfactor_stance_sigma1 = cf.Value("Predictive support polygon settings", "wfactor_stance_sigma1");
        this->wfactor_stance_sigma2 = cf.Value("Predictive support polygon settings", "wfactor_stance_sigma2");
        this->wfactor_swing_sigma1 = cf.Value("Predictive support polygon settings", "wfactor_swing_sigma1");
        this->wfactor_swing_sigma2 = cf.Value("Predictive support polygon settings", "wfactor_swing_sigma2");
        this->wfactor_offset = cf.Value("Predictive support polygon settings", "wfactor_offset");


        this->max_abs_vel_x = cf.Value("Control settings", "max_abs_vel_x");
        this->max_abs_vel_y= cf.Value("Control settings", "max_abs_vel_y");
        this->max_abs_vel_z = cf.Value("Control settings", "max_abs_vel_z");
        this->max_abs_vel_roll  = cf.Value("Control settings", "max_abs_vel_roll");
        this->max_abs_vel_pitch = cf.Value("Control settings", "max_abs_vel_pitch");
        this->max_abs_vel_yaw   = cf.Value("Control settings", "max_abs_vel_yaw");



        this->enable_force_controller = cf.Value("Force controller settings", "enable_force_controller");
        float k_pos = cf.Value("Force controller settings", "force_controller_k_base_position");
        float k_vel = cf.Value("Force controller settings", "force_controller_k_base_velocity");
        float k_rot = cf.Value("Force controller settings", "force_controller_k_base_angles");
        float k_ang_vel = cf.Value("Force controller settings", "force_controller_k_base_angular_vel");
        float w_dyn_model = cf.Value("Force controller settings", "force_controller_dynamical_model_weight");
        float w_forces = cf.Value("Force controller settings", "force_controller_regularization_weight");
        float w_prev_forces = cf.Value("Force controller settings", "force_controller_prev_force_weight");
        float force_controller_min_force = cf.Value("Force controller settings", "force_controller_min_force");
        float force_controller_max_force = cf.Value("Force controller settings", "force_controller_max_force");

        this->torqController->setProportionalConstController(k_pos, k_vel, k_rot, k_ang_vel);
        this->torqController->setWeightsController(w_dyn_model, w_forces, w_prev_forces);
        this->torqController->setFeetForceLimits(force_controller_min_force, force_controller_max_force);

        this->torqStaticController->setProportionalConstController(k_pos, k_vel, k_rot, k_ang_vel);
        this->torqStaticController->setWeightsController(w_dyn_model, w_forces, w_prev_forces);
        this->torqStaticController->setFeetForceLimits(force_controller_min_force, force_controller_max_force);


        /******************* PMC ***********************/
        this->enable_pmc_controller = cf.Value("Predictive Model controller settings", "enable_pmc_controller");

        float pcm_w_forces = cf.Value("Predictive Model controller settings", "pmc_controller_w_forces");


        this->predModelController->setHorizon_and_dtime( cf.Value("Predictive Model controller settings", "horizon"),
                                                         cf.Value("Predictive Model controller settings", "deltat"));


        Eigen::Vector3f pmc_base_pos_weight, pmc_base_ang_weight, pmc_base_vel_weight, pmc_base_ang_vel_weight;
        pmc_base_pos_weight     <<  cf.Value("Predictive Model controller settings", "pmc_controller_w_base_position_x"),
                                    cf.Value("Predictive Model controller settings", "pmc_controller_w_base_position_y"),
                                    cf.Value("Predictive Model controller settings", "pmc_controller_w_base_position_z");

        pmc_base_ang_weight     <<  cf.Value("Predictive Model controller settings", "pmc_controller_w_base_angles_x"),
                                    cf.Value("Predictive Model controller settings", "pmc_controller_w_base_angles_y"),
                                    cf.Value("Predictive Model controller settings", "pmc_controller_w_base_angles_z");

        pmc_base_vel_weight     <<  cf.Value("Predictive Model controller settings", "pmc_controller_w_base_vel_x"),
                                    cf.Value("Predictive Model controller settings", "pmc_controller_w_base_vel_y"),
                                    cf.Value("Predictive Model controller settings", "pmc_controller_w_base_vel_z");

        pmc_base_ang_vel_weight <<  cf.Value("Predictive Model controller settings", "pmc_controller_w_base_ang_vel_x"),
                                    cf.Value("Predictive Model controller settings", "pmc_controller_w_base_ang_vel_y"),
                                    cf.Value("Predictive Model controller settings", "pmc_controller_w_base_ang_vel_z");

        this->predModelController->setWeightsController(pmc_base_pos_weight,
                                                        pmc_base_ang_weight,
                                                        pmc_base_vel_weight,
                                                        pmc_base_ang_vel_weight,
                                                        pcm_w_forces);

        float pmc_min_force = cf.Value("Predictive Model controller settings", "pmc_controller_min_force");
        float pmc_max_force = cf.Value("Predictive Model controller settings", "pmc_controller_max_force");

        this->predModelController->setFeetForceLimits(pmc_min_force, pmc_max_force);

    }

    if (this->walk != walk_) {
        this->walk = walk_;
        if (this->walk) {
            this->initPhase();
            this->fl_trajectory.setWalking(true);
            this->fr_trajectory.setWalking(true);
            this->bl_trajectory.setWalking(true);
            this->br_trajectory.setWalking(true);
        }
        else{
            this->fl_trajectory.setWalking(false);
            this->fr_trajectory.setWalking(false);
            this->bl_trajectory.setWalking(false);
            this->br_trajectory.setWalking(false);
        }

    }
    std::cout << "SET GAIT: " << file << std::endl;


}


void GaitControl::setSequenceFromFile(std::string file){
    std::string file_dir = ros::package::getPath("gait_control") + std::string("/sequences/") + file + std::string(".sequence");

    std::ifstream infile(file_dir);
    if(!infile.good()){
        ROS_ERROR("Gait file does not exist, remember not to include extension '.sequence' in the name");
        return;
    }

    ConfigFile cf(file_dir);



    /********* get number of positions in sequence *********/
    std::string position_i;
    int num_positions = 0;
    while(true){
        position_i = std::string("position ") + std::to_string(num_positions);
        try{
            float base_offset_x_ = cf.Value(position_i, "base_offset_x");
        }
        catch (...){
            break;
        }
        num_positions += 1;
    }
    std::cout << "num positions " << num_positions << std::endl;

    /**********************************************************/


    double dt;
    float time_to_complete;
    ros::Time positionTimer;

    float final_base_offset_x;
    float final_base_offset_y;
    float vel_base_x;
    float vel_base_y;
    float vel_base_z;



    for(int i=0; i < num_positions; i++){
        position_i = std::string("position ") + std::to_string(i);

        time_to_complete = cf.Value(position_i, "time_to_complete");

        positionTimer = ros::Time::now();

        Eigen::Vector3f estimated_base_pos;
        this->stateEstimation->getBasePosition(&estimated_base_pos);

        vel_base_x = ( cf.Value(position_i, "base_offset_x") - estimated_base_pos.x())/time_to_complete;
        vel_base_x = ( cf.Value(position_i, "base_offset_y") - estimated_base_pos.y())/time_to_complete;
        vel_base_z = (cf.Value(position_i, "base_height")    - estimated_base_pos.z())/time_to_complete;


        bool stance_fl = cf.Value(position_i, "stance_fl");
        bool stance_fr = cf.Value(position_i, "stance_fr");
        bool stance_bl = cf.Value(position_i, "stance_bl");
        bool stance_br = cf.Value(position_i, "stance_br");

        int num_stance_legs = stance_fl + stance_fr + stance_bl + stance_br;



        /************ GET FINAL FEET POS (FROM REF) **************/
        Eigen::Vector3f final_pos_fl, final_pos_fr, final_pos_bl, final_pos_br;
        final_pos_fl << cf.Value(position_i, "feet_pos_fl_x"), cf.Value(position_i, "feet_pos_fl_y"), cf.Value(position_i, "feet_pos_fl_z");
        final_pos_fr << cf.Value(position_i, "feet_pos_fr_x"), cf.Value(position_i, "feet_pos_fr_y"), cf.Value(position_i, "feet_pos_fr_z");
        final_pos_bl << cf.Value(position_i, "feet_pos_bl_x"), cf.Value(position_i, "feet_pos_bl_y"), cf.Value(position_i, "feet_pos_bl_z");
        final_pos_br << cf.Value(position_i, "feet_pos_br_x"), cf.Value(position_i, "feet_pos_br_y"), cf.Value(position_i, "feet_pos_br_z");

        /************ GET CURRENT ANGLES **************/
        Eigen::Vector3f initial_angles_fl, initial_angles_fr, initial_angles_bl, initial_angles_br;
        this->getLegStateAngles("fl", &initial_angles_fl);
        this->getLegStateAngles("fr", &initial_angles_fr);
        this->getLegStateAngles("bl", &initial_angles_bl);
        this->getLegStateAngles("br", &initial_angles_br);

        /************ GET INITIAL FEET POS (FROM REF) **************/
        Eigen::Vector3f initial_pos_fl, initial_pos_fr, initial_pos_bl, initial_pos_br;
        this->kinematics->feetPosFromRef("fl", initial_angles_fl, &initial_pos_fl);
        this->kinematics->feetPosFromRef("fr", initial_angles_fr, &initial_pos_fr);
        this->kinematics->feetPosFromRef("bl", initial_angles_bl, &initial_pos_bl);
        this->kinematics->feetPosFromRef("br", initial_angles_br, &initial_pos_br);

        /************ GET VELOCITY TO MOVE FEET *******************/
        Eigen::Vector3f vel_fl, vel_fr, vel_bl, vel_br;
        vel_fl << (final_pos_fl - initial_pos_fl)/time_to_complete;
        vel_fr << (final_pos_fr - initial_pos_fr)/time_to_complete;
        vel_bl << (final_pos_bl - initial_pos_bl)/time_to_complete;
        vel_br << (final_pos_br - initial_pos_br)/time_to_complete;

        Eigen::Vector3f curr_pos_fl, curr_pos_fr, curr_pos_bl, curr_pos_br;
        Eigen::Vector3f curr_angles_fl, curr_angles_fr, curr_angles_bl, curr_angles_br;

        bool hr_fl, hr_fr, hr_bl, hr_br;

        while(true){
            dt = (ros::Time::now() - positionTimer).toSec();
            if (dt > time_to_complete){
                break;
            }



            //this->base_offset_y = cf.Value(position_i, "base_offset_y");
            if (num_stance_legs > 0){
                //this-> vel_cmd.twist.linear.x = vel_base_x;
                //this-> vel_cmd.twist.linear.y = vel_base_y;
                //this-> vel_cmd.twist.linear.z = vel_base_z;


                //this->mainNodeThread();
            }


            curr_pos_fl = initial_pos_fl + vel_fl * dt;
            curr_pos_fr = initial_pos_fr + vel_fr * dt;
            curr_pos_bl = initial_pos_bl + vel_bl * dt;
            curr_pos_br = initial_pos_br + vel_br * dt;


            hr_fl = this->kinematics->IK_leg_fromRef("fl", curr_pos_fl, &curr_angles_fl);
            hr_fr = this->kinematics->IK_leg_fromRef("fr", curr_pos_fr, &curr_angles_fr);
            hr_bl = this->kinematics->IK_leg_fromRef("bl", curr_pos_bl, &curr_angles_bl);
            hr_br = this->kinematics->IK_leg_fromRef("br", curr_pos_br, &curr_angles_br);



            /******************* SEND JOINT COMMANDS ************/
            if((hr_fl)&&(hr_fr)&&(hr_bl)&&(hr_br)) {    // if IK Reachability is possible



                this->joint_control_msg.position = {curr_angles_fl.x(), curr_angles_fl.y(), curr_angles_fl.z(),
                                                    curr_angles_fr.x(), curr_angles_fr.y(), curr_angles_fr.z(),
                                                    curr_angles_bl.x(), curr_angles_bl.y(), curr_angles_bl.z(),
                                                    curr_angles_br.x(), curr_angles_br.y(), curr_angles_br.z()};


            }

            this->publishData();
            ros::spinOnce();



        }




        std::cout << "completed time position " << i << std::endl;


    }

    std::cout << "SET SEQUENCE: " << file << std::endl;
}














void GaitControl::debug(std::string type, std::string msg){
	std::cout << type << ": " << msg << std::endl;
}


int main(int argc, char** argv){
	ros::init(argc, argv,"gait_control_node");
	ros::NodeHandle* nh = new ros::NodeHandle();
	ros::Rate loop_rate(10);
	GaitControl node(nh);


	while (ros::ok()){
		node.mainNodeThread();
		ros::spinOnce();
		//loop_rate.sleep();  //comment this line for fast!
	}
	return 0;
}
