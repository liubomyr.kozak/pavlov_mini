//
// Created by andres on 20.03.21.
//

#ifndef SRC_PREDICTIVE_MODEL_CONTROL_H
#define SRC_PREDICTIVE_MODEL_CONTROL_H


#include "eigen-qp.h"
#include <sstream>
#include <stdio.h>
#include <iostream>
#include <string>
#include <urdf/model.h>
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/QR>
#include <unsupported/Eigen/MatrixFunctions>
#include <eigen3/Eigen/Dense>
#include "feet_trajectory.h"
#include "kinematics.h"
#include "settings.h"
#include <math.h>

#include <tf_conversions/tf_eigen.h>
#include <tf/tf.h>
#include "QuadProgPP.h"
#include <qpOASES.hpp>

#define DYN_STATE_DIM 15


typedef Eigen::Matrix<float, DYN_STATE_DIM, DYN_STATE_DIM> Matrix13f;
typedef Eigen::Matrix<float, DYN_STATE_DIM, 12> Matrix13x12f;
typedef Eigen::Matrix<float, DYN_STATE_DIM, 1> Vector13f;
//typedef Eigen::Matrix<float, DYN_STATE_DIM * HORIZON, 1> Vector13kf;

USING_NAMESPACE_QPOASES

class PredictiveModeController {
public:
    PredictiveModeController(FeetTrajectory* fl_trajectory, FeetTrajectory* fr_trajectory, FeetTrajectory* bl_trajectory, FeetTrajectory* br_trajectory, Kinematics* kinematics);
    ~PredictiveModeController();

    void setHorizon_and_dtime(int horizon, float dt);
    void setTerrainPlane(std::string leg, float mu, const Eigen::Vector3f &normal, const Eigen::Vector3f &tangent1, const Eigen::Vector3f &tangent2);
    void setFeetForceLimits(float min_force, float max_force);
    void setWeightsController(const Eigen::Vector3f &base_pos_weight, const Eigen::Vector3f &base_ang_weight, const Eigen::Vector3f &base_vel_weight, const Eigen::Vector3f &base_ang_vel_weight,  float w_forces);

    void setRobotState(const Eigen::Vector3f base_pos, const Eigen::Vector3f base_vel, const Eigen::Vector3f base_ang, const Eigen::Vector3f base_ang_vel);
    void setRobotControl(const Eigen::Vector3f base_vel, const Eigen::Vector3f des_base_ang_vel);


    void computeForces(double current_main_phase);
    void computeForcesQPoases(double current_main_phase);


    void getForces(Eigen::Vector3f* force_fl, Eigen::Vector3f* force_fr, Eigen::Vector3f* force_bl, Eigen::Vector3f* force_br);
    void getBaseVelWeight(Eigen::Vector3f* weight){weight->operator=( this->base_vel_weight );};

private:
void getRz(float yaw, Eigen::Matrix3f* Rz);
void getCrossProductMatrix(Eigen::Vector3f r, Eigen::Matrix3f* C);

void getAmatrix(float yaw, const Eigen::Matrix3f &InertiaInv,
                const Eigen::Vector3f &pos_fl,
                const Eigen::Vector3f &pos_fr,
                const Eigen::Vector3f &pos_bl,
                const Eigen::Vector3f &pos_br,
                bool*  stance_feets,
                Matrix13f* A);

void getBmatrix(float yaw,
                const Eigen::Matrix3f &InertiaInv,
                const Eigen::Vector3f &pos_fl,
                const Eigen::Vector3f &pos_fr,
                const Eigen::Vector3f &pos_bl,
                const Eigen::Vector3f &pos_br,
                Matrix13x12f* B);

void getAdynMatrix(float yaw,  float dt, const Eigen::Matrix3f &InertiaInv,
                   const Eigen::Vector3f &pos_fl,
                   const Eigen::Vector3f &pos_fr,
                   const Eigen::Vector3f &pos_bl,
                   const Eigen::Vector3f &pos_br,
                   bool*  stance_feets,
                   Matrix13f* Adyn);

void getBdynMatrix(float yaw, float dt,
                   const Eigen::Matrix3f &InertiaInv,
                   const Eigen::Vector3f &pos_fl,
                   const Eigen::Vector3f &pos_fr,
                   const Eigen::Vector3f &pos_bl,
                   const Eigen::Vector3f &pos_br,
                   Matrix13x12f* Bdyn);

void getAdynAndBdynMatrices(float dt, const Matrix13f &A, const Matrix13x12f &B, Matrix13f *Adyn, Matrix13x12f* Bdyn);

private:

    int   HORIZON;
    float DELTAT;

    Eigen::MatrixXf state_ref;

    //Vector13kf state_ref;
    Vector13f  initial_state;

    Eigen::Vector3f base_pos;
    Eigen::Vector3f base_ang;
    Eigen::Vector3f base_vel;
    Eigen::Vector3f base_ang_vel;

    Eigen::Vector3f base_vel_weight;


    Eigen::Matrix3f I3;
    Eigen::Matrix3f O3;

    Eigen::Matrix<float, 1, 3> O3H;
    Eigen::Matrix<float, 3, 1> O3V;

    Eigen::Matrix3f Inertia;

    FeetTrajectory* fl_trajectory;
    FeetTrajectory* fr_trajectory;
    FeetTrajectory* bl_trajectory;
    FeetTrajectory* br_trajectory;

    Kinematics*              kinematics;


    Eigen::Matrix<float,5,3> terrain_fl;
    Eigen::Matrix<float,5,3> terrain_fr;
    Eigen::Matrix<float,5,3> terrain_bl;
    Eigen::Matrix<float,5,3> terrain_br;

    Eigen::Matrix<float, 5,1> d_min;
    Eigen::Matrix<float, 5,1> d_max;

    Matrix13f w_dyn_model;
    float w_forces;

    bool stance_fl;
    bool stance_fr;
    bool stance_bl;
    bool stance_br;
    Eigen::VectorXf curr_forces;


    SQProblem* solve_qp;
    bool init_qp;
};



#endif //SRC_PREDICTIVE_MODEL_CONTROL_H
