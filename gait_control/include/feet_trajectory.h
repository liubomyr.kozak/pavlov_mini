//
// Created by andres on 13.10.20.
//

#ifndef SRC_FEET_TRAJECTORY_H
#define SRC_FEET_TRAJECTORY_H

#include <eigen3/Eigen/Core>
#include <sstream>
#include <stdio.h>
#include <iostream>
#include <string>
#include <cmath>
#include "kinematics.h"
#include "settings.h"
#include "ros/ros.h"

enum FEET_STATE{
    STANCE,
    SWING
};

enum STOP_REQUEST_STATE{
    NON_STOP_REQUEST,
    REQUESTED_TO_STOP,
    MADE_LAST_STEP
};


class FeetTrajectory {
public:
    FeetTrajectory(std::string name);
    ~FeetTrajectory();

    void getFeetState(double main_phase);
    void makeStep(const Eigen::Vector3f &leg_des_dir,
                  const Eigen::Vector3f &des_base_vel,
                  const Eigen::Vector3f &base_vel,
                  double main_phase,
                  float base_height,
                  Eigen::Vector3f *feet_pos);

    void setTrajectoryParams(double period,
                             double prop_time_swing,
                             double phase_offset,
                             float height,
                             float feet_offset_x,
                             float feet_offset_y,
                             float base_offset_x,
                             float base_offset_y);


    void setWalking(bool walking);

    float getWeightingFactor(float sigma1, float sigma2, float sigma1_, float sigma2_, float offset);
    bool getStance();

    // This function is used by the predictive model controller
    bool predictFeetState(double current_main_phase, double dt, const Eigen::Vector3f &des_base_vel, Eigen::Vector3f* predicted_feet_pos);

public:
    std::string leg_name;
    double phase_offset;
    double leg_phase;
    double feet_state_phase;
    FEET_STATE feet_state;

    STOP_REQUEST_STATE stop_request_state;
    bool walking;

    Eigen::Vector3f initial_pos;
    Eigen::Vector3f current_pos;
    Eigen::Vector3f middle_pos;
    Eigen::Vector3f final_pos;
    bool set_initial_pos;

    double prop_time_swing;

    double period;
    float height;
    ros::Time timer;

    Eigen::Vector3f feet_pos_offset;
    Eigen::Vector3f base_pos_offset;

    Eigen::Vector3f prev_feet_vel;


    float max_leg_displacement;

};


#endif //SRC_FEET_TRAJECTORY_H
