#ifndef _gait_control_node_
#define _gait_control_node_


#include "ros/ros.h"
#include <ros/package.h>

#include "std_msgs/String.h"
#include "tf/transform_broadcaster.h"
#include "tf/transform_listener.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/TwistStamped.h"
#include "geometry_msgs/Point.h"
#include "sensor_msgs/JointState.h"

#include <sstream>
#include <stdio.h>
#include <iostream>
#include <string>
#include <chrono>
#include <fstream>

#include "pavlov_mini_msgs/robot_state.h"
#include "pavlov_mini_msgs/set_gait.h"
#include "pavlov_mini_msgs/feet_pressure.h"
#include "sensor_msgs/Imu.h"
#include "feet_trajectory.h"
#include "kinematics.h"
#include "feet_trajectory.h"
#include "polygon_support.h"
#include "torque_controller.h"
#include "predictive_model_control.h"
#include "contact_detection.h"
#include "state_estimation.h"
#include "ConfigFile.h"
#include <tf2/LinearMath/Quaternion.h>
#include "tf/transform_datatypes.h"
#include <tf_conversions/tf_eigen.h>
#include <cmath>
#include <eigen3/Eigen/Core>
#include "settings.h"
#include <tf/tf.h>


// [publisher subscriber headers]

// [service client headers]

// [action server client headers]

/**
 * \brief Specific Algorithm Class
 *
 */
class GaitControl
{
  private:

	ros::NodeHandle* nh;
    
    // [publisher attributes]
	ros::Publisher jointControlPub;
    ros::Publisher feetPressurePub;
    ros::Publisher feetForceControlPub;
    // [subscriber attributes]
	ros::Subscriber robotStateSub;
    ros::Subscriber torsoControlSub;
    ros::Subscriber velCmdSub;
    ros::Subscriber jointStateSub;
    ros::Subscriber imuDataSub;



    // [service attributes]
    ros::ServiceServer setGaitService;
    ros::ServiceServer setSequenceService;

    // [client attributes]

    // [action server attributes]

    // [action client attributes]

    // [transforms]
	//tf::TransformBroadcaster broadcaster;
		

    /********** private methods ************/

	void debug(std::string type, std::string msg);

	void initPhase();
    void getPhase(double period);
    void publishData();
    void setGaitFromFile(std::string file);
    void setSequenceFromFile(std::string file);

    /************ callbacks ************/
	void robotStateCB(const pavlov_mini_msgs::robot_state::ConstPtr &msg);
    void jointStateCB(const sensor_msgs::JointState::ConstPtr &msg);
    void imuDataCB(const sensor_msgs::Imu::ConstPtr &msg);
    void velCmdCB(const geometry_msgs::TwistStamped::ConstPtr &msg);
    void torsoControlCB(const geometry_msgs::PoseStamped::ConstPtr &msg);
    void getLegStateAngles (std::string leg, Eigen::Vector3f* angles);
    void getLegStateTorques(std::string leg, Eigen::Vector3f* torques);
    bool setGaitCB(pavlov_mini_msgs::set_gait::Request &req, pavlov_mini_msgs::set_gait::Response &res);
    bool setSequenceCB(pavlov_mini_msgs::set_gait::Request &req, pavlov_mini_msgs::set_gait::Response &res);


public:
   /**
    * \brief Constructor
    *
    * This constructor initializes specific class attributes and all ROS
    * communications variables to enable message exchange.
    */
	GaitControl(ros::NodeHandle* n);

   /**
    * \brief Destructor
    *
    * This destructor frees all necessary dynamic memory allocated within this
    * this class.
    */
    ~GaitControl();

  public:
   /**
    * \brief main node thread
    *
    * This is the main thread node function. Code written here will be executed
    * in every node loop while the algorithm is on running state. Loop frequency
    * can be tuned by modifying loop_rate attribute.
    *
    * Here data related to the process loop or to ROS topics (mainly data structs
    * related to the MSG and SRV files) must be updated. ROS publisher objects
    * must publish their data in this process. ROS client servers may also
    * request data to the corresponding server topics.
    */
    void mainNodeThread();


private:
    tf::TransformBroadcaster tf_br;

    pavlov_mini_msgs::robot_state robot_state;
    sensor_msgs::JointState       joint_state;
    geometry_msgs::TwistStamped   vel_cmd;
    geometry_msgs::PoseStamped    body_pose_cmd;
    geometry_msgs::PoseStamped    body_pose;
    sensor_msgs::JointState       joint_control_msg;
    sensor_msgs::Imu              imu_data;
    pavlov_mini_msgs::feet_pressure feet_press;
    pavlov_mini_msgs::feet_pressure feet_forces_msg;

    ros::Time phaseTimer;
    ros::Time publishControlTimer;
    ros::Time publishVisTimer;

    ros::Time dtTimer;
    ros::Time dtTimer_IMU;


    unsigned long phase_counter;
    double phase;

    Kinematics*                kinematics;
    PredPolygonSupport*        polygonSupport;
    ContactDetection*          contactDetector;
    torqueController*          torqController;
    torqueController*          torqStaticController;
    PredictiveModeController*  predModelController;
    PredictiveModeController*  predModelControllerNormal;

    StateEstimation*    stateEstimation;

    FeetTrajectory fl_trajectory;
    FeetTrajectory fr_trajectory;
    FeetTrajectory bl_trajectory;
    FeetTrajectory br_trajectory;

    bool walk;
    bool set_initial_gait_file;
    bool desired_walk;
    float gait_period;

    float factor_predictive_support_polygon;
    float wfactor_stance_sigma1;
    float wfactor_stance_sigma2;
    float wfactor_swing_sigma1;
    float wfactor_swing_sigma2;
    float wfactor_offset;
    float feet_offset_x;
    float feet_offset_y;
    float base_offset_x;
    float base_offset_y;
    float force_contact_threshold;
    float capture_point_velocity_factor;

    float max_abs_vel_x;
    float max_abs_vel_y;
    float max_abs_vel_z;
    float max_abs_vel_roll;
    float max_abs_vel_pitch;
    float max_abs_vel_yaw;

    float robot_COM_z_pos;



    bool enable_force_controller;
    bool enable_pmc_controller;

    bool available_imu_data;

    long loop_counter;

};

#endif

